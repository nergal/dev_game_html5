
// NOTE: This file is only executed for Debug builds! Release builds 
// will ONLY load the baked game.min.js - containing ios-impact.js and
// all files you required from lib/game/main.js

// The game.min.js is automatically created when you compile a release 
// build. See tools/ios-bake.sh

ios.require( 'lib/impact/impact.js' );
ios.require( 'lib/game/main.js' );
