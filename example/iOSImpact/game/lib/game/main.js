ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
	
	'game.entities.player',
	'game.entities.spike',
	'game.levels.test',
	
	// require all ios plugins for compatability
	'plugins.ios.ios'
)
.defines(function(){

MyGame = ig.Game.extend({
	
	gravity: 300, // All entities are affected by this
	
	// Load a font
	font: new ig.Font( 'media/04b03.font.png' ),
	
	// button images
	buttons: new ig.Image( 'media/buttons.png' ),
	
	init: function() {
		// Bind keys
		if( ios ) {
			// When running on ios we have to specify the buttons by
			// defining an area: (x, y, width, height, action)
			// Note that these are unscaled pixel coordinates!
			ig.input.bindTouchArea( 0, 224, 80, 96, 'left' );
			ig.input.bindTouchArea( 80, 224, 80, 96, 'right' );
			ig.input.bindTouchArea( 320, 224, 80, 96, 'shoot' );
			ig.input.bindTouchArea( 400, 224, 80, 96, 'jump' );
		}
		else {
			ig.input.bind( ig.KEY.LEFT_ARROW, 'left' );
			ig.input.bind( ig.KEY.RIGHT_ARROW, 'right' );
			ig.input.bind( ig.KEY.X, 'jump' );
			ig.input.bind( ig.KEY.C, 'shoot' );
		}
		
		// Load the LevelTest as required above ('game.level.test')
		this.loadLevel( LevelTest );
	},
	
	loadLevel: function( data ) {
		this.parent( data );
		
		// Enable the pre-rendered mode for all background maps
		for( var i = 0; i < this.backgroundMaps.length; i++ ) {
			this.backgroundMaps[i].chunkSize = 256;
			this.backgroundMaps[i].preRender = true;
		}
	},
	
	update: function() {		
		// Update all entities and BackgroundMaps
		this.parent();
		
		// screen follows the player
		var player = this.getEntitiesByType( EntityPlayer )[0];
		if( player ) {
			this.screen.x = player.pos.x-ig.system.width/2;
			this.screen.y = player.pos.y-ig.system.height/2;
		}
	},
	
	draw: function() {
		// Draw all entities and BackgroundMaps
		this.parent();
		
		//this.font.draw( 'Arrow Keys, X, C', 2, 2 );
		
		// Draw buttons
		this.buttons.drawTile( 0, 110, 0, 80, 48, false, false );
		this.buttons.drawTile( ig.system.width-80, 110, 1, 80, 48, false, false );
	}
});

// This demo game has no sound, but if we are on iOS, load sound files
// with a .caf extension instead of .ogg or .mp3
if( ios ) {
	ig.Sound.use = [ig.Sound.FORMAT.CAF];
}

// Start the Game with 60fps, a resolution of 240x160, scaled
// up by a factor of 2
ig.main( '#canvas', MyGame, 60, 240, 160, 2 );

});
