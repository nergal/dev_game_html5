ig.module(
	'plugins.ios.system'
)
.requires(
	'impact.system'
)
.defines(function(){


ig.System.inject({
	_clearColor: '#000',
	
	init: function( canvasId, fps, width, height, scale ) {
		this.fps = fps;
		
		this.clock = new ig.Timer();
		this.canvas = {};
		
		this.nativeWidth = width * scale;
		this.nativeHeight = height * scale;
		
		this.resize( width, height, 1 );
		this.context = new native.ScreenCanvas( width, height, scale );
	},
	
	
	resize: function( width, height, scale ) {
		this.width = width;
		this.height = height;
		this.scale = scale || this.scale;
		
		this.realWidth = this.width * this.scale;
		this.realHeight = this.height * this.scale;
		this.canvas.width = this.realWidth;
		this.canvas.height = this.realHeight;
	},	
	
	
	clear: function( color ) {
		if( this._clearColor != color ) {
			this.context.clearStyle = color;
			this._clearColor = color;
		}
		this.context.clear();
	},
	
	
	run: function() {
		this.parent();
		this.context.present();
	},
	
	
	getDrawPos: function( p ) {
		return p;
	}
});


});