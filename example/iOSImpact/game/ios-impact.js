// Set up the global 'window' object
window = this; // Make 'window' the global scope

// An instance of iOSImpact provides basic utility functions, such as timers
// and device properties
ios = new native.iOSImpact();
localStorage = new native.LocalStorage();

devicePixelRatio = ios.devicePixelRatio;
if( ios.landscapeMode ) {
	innerWidth = ios.screenWidth;
	innerHeight = ios.screenHeight;
}
else {
	innerWidth = ios.screenWidth;
	innerHeight = ios.screenHeight;
}

screen = {
	availWidth: innerWidth,
	availHeight: innerHeight
};

navigator = {
	userAgent: ios.userAgent
};

// ios.log only accepts one param; console.log accepts multiple params
// and joins them
console = {
	log: function() {
		var args = Array.prototype.join.call(arguments, ', ');
		ios.log( args );
	}
};

setTimeout = function(cb, t){ return ios.setTimeout(cb, t); };
setInterval = function(cb, t){ return ios.setInterval(cb, t); };
clearTimeout = function(id){ return ios.clearTimeout(id); };
clearInterval = function(id){ return ios.clearInterval(id); };


// The native Audio class mimics the HTML5 Audio element; we
// can use it directly as a substitute 
Audio = native.Audio;


// Set up a fake HTMLElement and document object, so Impact is happy
HTMLElement = function( tagName ){ 
	this.tagName = tagName;
	this.children = [];
};

HTMLElement.prototype.appendChild = function( element ) {
	this.children.push( element );
	
	// If the child is a script element, begin loading it
	if( element.tagName == 'script' ) {
		var id = ios.setTimeout( function(){
			ios.require( element.src ); 
			if( element.onload ) {
				element.onload();
			}
		}, 1 );
	}
};

document = {
	location: { href: 'index' },
	
	head: new HTMLElement( 'head' ),
	body: new HTMLElement( 'body' ),
	
	createElement: function( name ) {
		if( name == 'canvas' ) {
			return new native.Canvas();
		}
		return new HTMLElement( 'script' );
	},
	
	getElementById: function( id ){	
		return null;
	},
	
	getElementsByTagName: function( tagName ){
		if( tagName == 'head' ) {
			return [document.head];
		}
	},
	
	addEventListener: function( type, callback ){
		if( type == 'DOMContentLoaded' ) {
			ios.setTimeout( callback, 1 );
		}
	}
};
addEventListener = function( type, callback ){};


