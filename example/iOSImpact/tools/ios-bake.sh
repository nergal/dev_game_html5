#!/bin/bash

# This script is only executed for Release Builds! See the 
# 'Run Script' phase for this Target
echo "Baking..."

# Path to impact.js and your game's main .js
IMPACT_LIBRARY=lib/impact/impact.js
GAME=lib/game/main.js

# Output file
OUTPUT_FILE=game.min.js
TEMP_FILE=temp.js

# Change CWD to Impact's base dir and bake!
cd ${PROJECT_DIR}/game/
php ../tools/bake.php $IMPACT_LIBRARY $GAME $TEMP_FILE

# Combine ios-impact.js and the baked file into 
# the final game.min.js file
cat ios-impact.js $TEMP_FILE > $OUTPUT_FILE
rm $TEMP_FILE