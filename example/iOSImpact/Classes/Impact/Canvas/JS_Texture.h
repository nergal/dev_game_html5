#import <Foundation/Foundation.h>
#import "JS_BaseClass.h"
#import "Texture.h"
#import "Drawable.h"

@interface JS_Texture : JS_BaseClass <Drawable> {
	NSString * path;
	Texture * texture;
}

@property (readonly) Texture * texture;

@end
