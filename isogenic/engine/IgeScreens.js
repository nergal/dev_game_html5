/** IgeScreens - The screen management class. {
	engine_ver:"0.1.0",
	category:"class",
} **/

/** beforeCreate - Fired before a screen is created. {
	category: "event",
	argument: {
		type:"object",
		name:"screen",
		desc:"The screen object this event was fired for.",
	},
} **/
/** afterCreate - Fired after a screen is created. {
	category: "event",
	argument: {
		type:"object",
		name:"screen",
		desc:"The screen object this event was fired for.",
	},
} **/
/** afterRemove - Fired after a screen is removed. {
	category: "event",
	argument: {
		type:"object",
		name:"screen",
		desc:"The screen object this event was fired for.",
	},
} **/
/** resize - Fired after screen(s) are resized. {
	category: "event",
} **/
IgeScreens = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** currentScreen - The current screen that the engine is displaying (internal use). {
		category:"property",
		type:"object",
	} **/
	currentScreen: null,
	
	/** renderList - An array with an integer index that contains a list of
	screens to actively render (internal use). {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	renderList: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeScreens';
		
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.renderList = [];
		
		this.collectionId = 'screen';
		
		if (!this.engine.isServer && !this.engine.isSlave) {
			// Set an event hook to resize screens and then any viewports with the autoresize property set to true
			window.addEventListener('resize', this.bind( function () { this._doAutoSize(); } ), false);
		}
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('screensCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('screensUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand('screensRemove', this.bind(this._remove));
		
		// Network screen commands
		this.engine.network.registerCommand('screensSetCurrent', this.bind(this.setCurrent));
		this.engine.network.registerCommand('screensStartSend', this.bind(this.netSendStarted));
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'screen_id',
			'screen_background_color',
			'screen_html',
			'screen_persist',
		]);
	},
	
	/** create - Create a new screen. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the newly created item on success or false on failure.",
		},
		argument: {
			type:"object",
			name:"newScreen",
			desc:"The screen object to be created.",
			link:"screenData",
		},
	} **/
	create: function (newScreen, callback) {
		// TO-DO - Make sure the callback is actually processed, it is not at present. Also, this doens't
		// follow the new networkItem propagation system!
		// Check if we need to auto-generate an id
		this.ensureIdExists(newScreen);
		
		this.emit('beforeCreate', newScreen);
		
		newScreen.$local = newScreen.$local || {};
		this.byIndex.push(newScreen);
		this.byId[newScreen.screen_id] = newScreen;
		
		if (!this.engine.isServer && !this.engine.isSlave) {
			var screenContainer = new IgeElement('div', {
				id:newScreen.screen_id,
			});
			
			screenContainer.style.position = 'absolute';
			screenContainer.style.width = '100%';
			screenContainer.style.height = '100%';
			screenContainer.style.display = 'none';
			if (newScreen.screen_background_color) { screenContainer.style.backgroundColor = newScreen.screen_background_color; }
			
			if (newScreen.screen_parent_id) {
				var parentElement = document.getElementById(newScreen.screen_parent_id);
				if (parentElement != null) {
					parentElement.appendChild(screenContainer);
				} else {
					this.log('Cannot append the screen element to the specified parent because the parent element does not exist.', 'error', newScreen);
				}
			} else {
				// No parent element_id specified so append to the body
				document.getElementsByTagName('body')[0].appendChild(screenContainer);
			}
			
			// Now if the screen has an HTML file as its contents, load that into the screen HTML
			if (newScreen.screen_html) {
				//this.log('Loading HTML data into screen container from ' + newScreen.screen_html + '...');
				$.ajax({
					url: newScreen.screen_html,
					success: function (data) { document.getElementById(newScreen.screen_id).innerHTML += data; },
					dataType: 'text/html'
				});
			}
			
			if (newScreen.screen_always_render) { this.renderList.push(newScreen); }
		}
		
		this.emit('afterCreate', newScreen);
		
		return newScreen;
		
	},
	
	/** update - Updates the class collection item with the matching id specified in the updData
	parameter with the properties in the props parameter. If no props is passed, the item is updated
	with all properties of the updData parameter. {
		category:"method",
		engine_ver:"0.1.4",
		return: {
			type:"bool",
			desc:"Returns false on failure or the updated item on success.",
		},
		arguments: [{
			type:"object",
			name:"updData",
			desc:"The object containing update data.",
		}, {
			type:"array",
			name:"props",
			desc:"If set, will limit the update to the target screen to the property names in the array.",
			flags:"optional",
		}],
	} **/
	update: function (updData, props) {
		if (updData != null) {
			if (updData.screen_id) {
				var curScreen = this.byId[updData.screen_id];
				this.emit('beforeUpdate', curScreen);
				
				// Update the current object with the new object's properties
				if (!props) {
					for (var i in updData) {
						curScreen[i] = updData[i];
					}
				} else {
					for (var i in props) {
						curScreen[props[i]] = updData[props[i]];
					}
				}
				
				this.emit('afterUpdate', curScreen);
				
				return curScreen;
				
			} else {
				// Cannot update entity because it has no id!
				this.log('Cannot update item because passed updData has no screen_id property.', 'error', updData);
				return false;
			}
		} else {
			this.log('Cannot update item because the passed updData is a null object.', 'error', updData);
			return false;
		}
		
	},
	
	/** remove - Removes a screen from the engine. {
		category:"method",
		engine_ver:"0.1.4",
		return: {
			type:"bool",
			desc:"Returns false on failure or null for any other reason.",
		},
		arguments: [{
			type:"object",
			name:"curScreen",
			desc:"The screen object to be removed.",
			link:"screenData",
		}, {
			type:"method",
			name:"callback",
			desc:"The method to call when the entity has been successfully removed from the engine.",
			flags:"optional",
		}],
	} **/
	remove: function (curScreen, callback) {
		var curScreen = this.read(curScreen);
		
		// Check if a callback was specified and if so, attach it to the item
		if (typeof callback == 'function') {
			curScreen.$local.$callback = callback;
		}
		
		this._propagate(curScreen, PROPAGATE_DELETE);
	},
	
	/** _remove - Private method that removes the item identified by the passed
	id from the engine. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"object",
			name:"curScreen",
			desc:"The item to be removed.",
			link:"screenData",
		}],
	} **/
	_remove: function (curScreen) {
		var curScreen = this.read(curScreen);
		
		if (curScreen && curScreen != null) {
			
			// Check if the curScreen has a callback method and if so, save a reference to it, then fire after removal of the curScreen
			var cbMethod = null;
			if (typeof curScreen.$local.$callback == 'function') {
				cbMethod = curScreen.$local.$callback;
			}
			
			// Remove it from all the arrays
			var arr1 = this.byId[curScreen.screen_id];
			var arr2 = this.byIndex;
			
			// Remove the screem from all the lookup arrays
			delete this.byId[curScreen.entity_id];
			var arr1Index = arr1.indexOf(curScreen);
			var arr2Index = arr2.indexOf(curScreen);
			arr1.splice(arr1Index, 1);
			arr2.splice(arr2Index, 1);
			
			if (typeof cbMethod == 'function') {
				cbMethod.apply(this);
			}
			
			// TO-DO - Remove all HTML elements related to the screen
			
			return true;
			
		} else {
			return false;
		}
		
	},
	
	/** setCurrent - Set the current visible screen and hide all others. {
		category:"method",
		engine_ver:"0.1.2",
		return: {
			type:"bool",
			desc:"Returns false on failure or true on success.",
		},
		arguments: [{
			type:"string",
			name:"screenId",
			desc:"The id of the screen to make current.",
		}, {
			type:"object",
			name:"client",
			desc:"The socket.io client to send the command to.",
			flags:"optional, server",
		}],
	} **/
	setCurrent: function (screenId, client) {
		if (this.engine.isServer) {
			/* CEXCLUDE */
			this.log('Setting current screen to id: ' + screenId);
			this.engine.network.send('screensSetCurrent', screenId, client);
			/* CEXCLUDE */
		} else if (!this.engine.isSlave) {
			// If there is a current screen, hide it before switching!
			if (this.currentScreen != null) {
				if (document.getElementById(this.currentScreen.screen_id) != null) {
					document.getElementById(this.currentScreen.screen_id).style.display = 'none';
					if (!this.currentScreen.screen_always_render && this.renderList.indexOf(this.currentScreen) > -1) { this.renderList.splice(this.renderList.indexOf(this.currentScreen), 1); }
				}
			}
			
			/* Go through all the maps on this screen and then all the viewports looking
			at those maps and make sure that all the viewports have their dirty rectangles
			deleted and one added that is the size of the viewport - this means that the
			viewports will be updated whenever switching screens but also stops massive
			lag when loads of entities have changed position or been created since the 
			screen was inactive. */
			
			// Check if a screen is actually currently selected
			if (this.byId[screenId] != null) {
				
				// Loop through all viewports assigned to this screen
				var viewportsByScreenId = this.engine.viewports.byScreenId[screenId];
				
				// Check if we have any viewports for this screen!
				if (viewportsByScreenId != null) {
					
					var viewportCount = viewportsByScreenId.length;
					
					// Select each viewport
					while (viewportCount--) {
						
						/* Make the whole viewport and all layers dirty so they will all redraw on the
						next rendering cycle */
						this.log('Making viewport dirty with id: ' + viewportsByScreenId[viewportCount].viewport_id);
						this.engine.viewports.makeViewportDirty(viewportsByScreenId[viewportCount]);
						
					}
					
				}
				
			}			
			
			// Assign the new screen as current and show it!
			this.currentScreen = this.byId[screenId];
			if (this.currentScreen != null) {
				document.getElementById(this.currentScreen.screen_id).style.display = 'block';
				this.renderList.push(this.currentScreen);
				this.log('Current screen set to ' + this.currentScreen.screen_id);
				return true;
			} else {
				this.log('An attempt was made to switch to a screen that does not exist named: ' + screenId, 'error');
				return false;
			}
		} else {
			// Engine is running as slave so set current screen and xmsg
			this.currentScreen = this.byId[screenId];
			return true;
		}
		
		return false;
	},
	
	/** _doAutoSize - Autosizes all screens to fill their parent elements. Called
	when the browser window resizes. {
		category:"method",
		engine_ver:"0.1.2",
	} **/
	_doAutoSize: function () {
		
		//var newWidth = window.innerWidth;
		//var newHeight = window.innerHeight;
		var screenCount = this.byIndex.length;
		var screenDef = null;
		var screenContainer = null;
		
		while (screenCount--) {
			
			screenDef = this.byIndex[screenCount];
			screenContainer = document.getElementById(screenDef.screen_id);
			
			var newWidth = $('#' + screenDef.screen_id).parent().width();
			var newHeight = $('#' + screenDef.screen_id).parent().height();
			
			if (screenContainer != null) {
				screenContainer.style.width = newWidth + 'px';
				screenContainer.style.height = newHeight + 'px';
				resized = true;
				
			}
			
		}
		
		this.emit('resize');
		
	},
	
});