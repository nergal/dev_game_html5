/** IgeCameras - The camera manager. {
	engine_ver:"0.1.0",
	category:"class",
} **/

/** beforeCreate - Fired before a camera is created. {
	category: "event",
	argument: {
		type:"object",
		name:"camera",
		desc:"The camera object this event was fired for.",
	},
} **/
/** afterCreate - Fired after a camera is created. {
	category: "event",
	argument: {
		type:"object",
		name:"camera",
		desc:"The camera object this event was fired for.",
	},
} **/
IgeCameras = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** tracking - An array with a string index holds data about which entities
	cameras are currently tracking. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	tracking: null,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeCameras';
		
		this.engine = engine;
		this.collectionId = 'camera';
		
		this.byIndex = [];
		this.byId = [];
		
		this.tracking = [];
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('camerasCreate', this.bind(this.receiveCreate));
		this.engine.network.registerCommand('camerasUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand('camerasRemove', this.bind(this.receiveRemove));
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'camera_id',
			'camera_x',
			'camera_y',
			'camera_z',
		]);
	},
	
	/** create - Create a new camera to be use with a viewport. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the newly created item on success or false on failure.",
		},
		argument: {
			type:"object",
			name:"camera",
			desc:"The camera object to be created.",
			link:"cameraData",
		},
	} **/
	create: function (camera) {
		this.emit('beforeCreate', camera);
		
		// Check if we need to auto-generate an id
		this.ensureIdExists(camera);
		
		camera.$local = camera.$local || {};
		// Make sure the camera scale is usable
		camera.camera_scale = camera.camera_scale || 1;
		
		this.byIndex.push(camera);
		this.byId[camera.camera_id] = camera;
		
		this.emit('afterCreate', camera);
		
		return camera;
	},
	
	update: function () {},
	remove: function () {},
	
	/** trackTarget - Tells the camera to start tracking the movement of a target entity. The camera
	will follow the target entity until cancelTrackTarget is used to cancel tracking. {
		category:"method",
		engine_ver:"0.1.1",
		arguments: [{
			type:"object",
			name:"camera",
			desc:"The camera to use when tracking the entitiy.",
		}, {
			type:"string",
			name:"entityId",
			desc:"The id of the entitiy to track.",
		}],
	} **/
	trackTarget: function (camera, entityId) {
		camera = this.read(camera);
		
		if (camera) {
			var entity = this.engine.entities.byId[entityId];
			
			if (entity) {
				// Start tracking the movement of this entity
				this.tracking[entityId] = this.tracking[entityId] || [];
				this.tracking[entityId].push(camera);
				this.engine.entities.on('afterMove', this.bind(this._targetMoved));
				
				// Get the cam to look at the entity
				this._targetMoved(entity);
			}
		}
	},
	
	/** cancelTrackTarget - Will cancel tracking of an entity previously set to track
	by a call to trackTarget. {
		category:"method",
		engine_ver:"0.1.1",
		arguments: [{
			type:"object",
			name:"camera",
			desc:"The camera to use when cancelling the active tracking.",
		}, {
			type:"string",
			name:"entityId",
			desc:"The id of the entitiy that is currently being tracked.",
		}],
	} **/
	cancelTrackTarget: function (camera, entityId) {
		var entity = this.engine.entities.byId[entityId];
		
		if (camera && entity) {
			if (this.tracking[entityId]) {
				// Find the camera in the entity tracking array
				var camIndex = this.tracking[entityId].indexOf(camera);
				
				// Remove the camara from the entity tracking array
				if (camIndex > -1) {
					// Remove cam
					this.tracking[entityId].splice(camIndex, 1);
					
					// Check if this entity tracking array is now empty
					if (!this.tracking[entityId].length) {
						delete this.tracking[entityId];
					}
					
					// Check if the entire tracking array is now empty
					if (!this.tracking.length) {
						// No more tracking so kill the event listener so we're not being polled needlessly
						this.engine.entities.events.cancelOn('afterMove', this.bind(this._targetMoved));
					}
				}
			}
		}
	},
	
	/** _targetMoved - Called when an entity currently being tracked emits an afterMove event. {
		category:"method",
		arguments: [{
			type:"object",
			name:"entity",
			desc:"The entity that just moved.",
		}],
	} **/
	_targetMoved: function (entity) {
		// Check for a tracking array for this entity
		if (this.tracking[entity.entity_id]) {
			// Loop through the list and update the tracking cams to look at this entity
			var camCount = this.tracking[entity.entity_id].length;
			
			while (camCount--) {
				var camera = this.tracking[entity.entity_id][camCount];
				
				// Get the map and calculate the real co-ordinates for where to look on screen
				var renderMode = entity.$local.$map.map_render_mode;
				var entPos = this.engine.entities.getPosition(entity);
				
				// Get the cam to look at the entity
				this.lookAt(camera, entPos[renderMode][0], entPos[renderMode][1]);
			}
		}
	},
	
	/** lookAt - Sets the camera position to center on the specified co-ordinates. {
		category:"method",
		arguments: [{
			type:"object",
			name:"camera",
			desc:"The camera to position.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate to center the camera on.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate to center the camera on.",
		}, {
			type:"integer",
			name:"z",
			desc:"The z co-ordinate to center the camera on.",
		}],
	} **/
	lookAt: function (camera, x, y, z) {
		camera = this.read(camera);
		
		if (x != camera.camera_x || y != camera.camera_y || z != camera.camera_z) {
			
			var xDiff = 0;
			var yDiff = 0;
			var zDiff = 0;
			
			if (x != camera.camera_x) { var xDiff = x - camera.camera_x; }
			if (y != camera.camera_y) { var yDiff = y - camera.camera_y; }
			if (z != camera.camera_z) { var zDiff = z - camera.camera_z; }
			
			//this.log('X: ' + xDiff + ', Y: ' + yDiff + ', Z: ' + zDiff);
			
			camera.camera_x = x;
			camera.camera_y = y;
			camera.camera_z = z;
			
			camera.camera_panning = false;
			
			if (camera.$local && !camera.$local.$viewport.viewport_hide) {
				this.engine.viewports.updateViewportPreCalc(camera.$local.$viewport);
				this.engine.viewports.clearDirtySections(camera.$local.$viewport);
				this.engine.viewports.markViewportSectionDirty(camera.$local.$viewport, xDiff, yDiff);
				this.engine.viewports._setCleanSectionMove(camera.$local.$viewport, xDiff, yDiff);
				this.engine.viewports.requestEntityData(camera.$local.$viewport);
				this.engine.viewports.makeViewportDirty(camera.$local.$viewport);
				
				this.emit('afterLookAt', camera);
			} else {
				this.log('Camera $local not available!', 'warning', camera);
			}
		}
	},
	
	/** lookBy - Sets the camera position to center on the specified co-ordinates by
	adding the passed co-ordinates to the current camera position. {
		category:"method",
		arguments: [{
			type:"object",
			name:"camera",
			desc:"The camera to position.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate to add to the current x position.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate to add to the current y position.",
		}, {
			type:"integer",
			name:"z",
			desc:"The z co-ordinate to add to the current z position.",
		}],
	} **/
	lookBy: function (camera, x, y, z) {
		camera = this.read(camera);
		
		if (x != 0 || y != 0 || z != 0) {
			var xDiff = x || 0;
			var yDiff = y || 0;
			var zDiff = z || 0;
			
			camera.camera_x += x;
			camera.camera_y += y;
			camera.camera_z += z;
			
			camera.camera_panning = false;
			
			if (camera.$local && !camera.$local.$viewport.viewport_hide) {
				this.engine.viewports.updateViewportPreCalc(camera.$local.$viewport);
				this.engine.viewports.clearDirtySections(camera.$local.$viewport);
				this.engine.viewports.markViewportSectionDirty(camera.$local.$viewport, xDiff, yDiff);
				this.engine.viewports._setCleanSectionMove(camera.$local.$viewport, xDiff, yDiff);
				this.engine.viewports.requestEntityData(camera.$local.$viewport);
				this.engine.viewports.makeViewportDirty(camera.$local.$viewport);
				
				this.emit('afterLookBy', camera);
			} else {
				this.log('Camera $local not available!', 'warning', camera);
			}		
		}
	},
	
	/** panTo - Pans the camera from its current position to the specified position
	in the speed passed in milliseconds. {
		category:"method",
		engine_ver:"0.3.0",
		arguments: [{
			type:"object",
			name:"camera",
			desc:"The camera to pan.",
		}, {
			type:"integer",
			name:"x",
			desc:"The x co-ordinate to center the camera on.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate to center the camera on.",
		}, {
			type:"integer",
			name:"z",
			desc:"The z co-ordinate to center the camera on.",
		}, {
			type:"integer",
			name:"speedMs",
			desc:"The time in milliseconds to finish the pan in.",
		}],
	} **/
	panTo: function (camera, x, y, z, timeMs) {
		if (x != camera.camera_x || y != camera.camera_y || z != camera.camera_z) {
			camera.camera_pan_x = x;
			camera.camera_pan_y = y;
			camera.camera_pan_z = z;
			camera.camera_pan_time = timeMs;
			
			camera.camera_panning = true;
			
			if (camera.$local) {
				this.engine.viewports.updateViewportPreCalc(camera.$local.$viewport);
				this.engine.viewports.requestEntityData(camera.$local.$viewport);
				this.engine.viewports.makeViewportDirty(camera.$local.$viewport);
				
				this.emit('afterPanTo', camera);
			} else {
				this.log('Camera $local not available!', 'warning', camera);
			}	
		}
	},
	
	/** setScale - Scales the camera's view. {
		category:"method",
		engine_ver:"0.3.0",
		arguments: [{
			type:"object",
			name:"camera",
			desc:"The camera to pan.",
		}, {
			type:"float",
			name:"newScale",
			desc:"The new scale value.",
		}],
	} **/
	setScale: function (camera, newScale) {
		if (camera.camera_scale != newScale) {
			// Make sure the scale is not a recurring float (stops graphics glitches)
			newScale = parseFloat(newScale.toFixed(2));
			console.log('newscale', newScale);
			// Set the new scale
			camera.camera_scale = newScale;
			
			if (camera.$local) {
				this.engine.viewports.updateViewportPreCalc(camera.$local.$viewport);
				this.engine.viewports.requestEntityData(camera.$local.$viewport);
				this.engine.viewports.makeViewportDirty(camera.$local.$viewport);
				
				this.emit('afterSetScale', camera);
			} else {
				this.log('Camera $local not available!', 'warning', camera);
			}	
		}
	},
	
});