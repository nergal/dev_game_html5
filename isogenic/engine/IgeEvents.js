/** IgeEvents - The events management class. {
	engine_ver:"0.0.5",
	category:"class",
} **/
IgeEvents = new IgeClass({
	
	/** eventListeners - An array of objects, each defining an event listener method
	and some properties. {
		category:"property",
		type:"array",
	} **/
	_eventListeners: [],
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
	} **/
	init: function () {	},
	
	/** on - Add an event listener method for an event. {
		category:"method",
		arguments: [{
			type:"multi",
			name:"eventName",
			desc:"The name of the event to listen for (string), or an array of events to listen for (multi-events in 0.2.0 or higher only).",
		}, {
			type:"method",
			name:"call",
			desc:"The method to call when the event listener is triggered.",
		}, {
			type:"object",
			name:"context",
			desc:"The context in which the call to the listening method will be made (sets the 'this' variable in the method to the object passed as this parameter).",
			flags:"optional",
		}, {
			type:"bool",
			name:"oneShot",
			desc:"If set, will instruct the listener to only listen to the event being fired once and will not fire again.",
			flags:"optional",
		}, {
			type:"bool",
			name:"sendEventName",
			desc:"If set, will instruct the emitter to send the event name as the argument instead of any emitted arguments. Only 0.2.0 or higher.",
			flags:"optional",
		}],
	} **/
	on: function (eventName, call, context, oneShot, sendEventName) {
		if (typeof call == 'function') {
			if (typeof eventName == 'string') {
				// Check if this event already has an array of listeners
				this._eventListeners[eventName] = this._eventListeners[eventName] || [];
				
				// Compose the new listener
				var newListener = {
					call: call,
					context: context,
					oneShot: oneShot,
					sendEventName: sendEventName,
				}
				
				// Check if we already have this listener in the list
				var addListener = true;
				
				// TO-DO - Could this do with using indexOf? Would that work? Would be faster.
				for (var i in this._eventListeners[eventName]) {
					if (this._eventListeners[eventName][i] == newListener) {
						addListener = false;
						break;
					}
				}
				
				// Add this new listener
				if (addListener) {
					//console.log('Adding event listener for event "' + eventName + '"');
					this._eventListeners[eventName].push(newListener); 
				}
				
				return newListener;
			} else {
				// The eventName is an array of names, creating a group of events
				// that must be fired to fire this event callback
				if (eventName.length) {
					// Loop the event array
					var multiEvent = [];
					multiEvent[0] = 0; // This will hold our event count total
					multiEvent[1] = 0; // This will hold our number of events fired
					multiEvent[2] = []; // This will hold the list of already-fired event names
					
					// Define the multi event callback
					multiEvent[3] = this.bind(function (firedEventName) {
						if (multiEvent[2].indexOf(firedEventName) == -1) {
							multiEvent[2].push(firedEventName);
							multiEvent[1]++;
							
							if (multiEvent[0] == multiEvent[1]) {
								call.apply(context || this);
							}
						}
					});
						
					for (var eventIndex in eventName) {
						var eventData = eventName[eventIndex];
						var eventObj = eventData[0];
						var eventNameArray = eventData[1];
						
						multiEvent[0] += eventNameArray.length;
						
						for (var singleEventIndex in eventNameArray) {
							// Get the event name
							var singleEventName = eventNameArray[singleEventIndex];
							
							// Register each event against the event object with a callback
							eventObj.on(singleEventName, multiEvent[3], null, true, true);
						}
					}
				}
			}
		} else {
			if (typeof eventName != 'string') {
				eventName = '*Multi-Event*'
			}
			this.log('Cannot register event listener for event "' + eventName + '" because the passed callback is not a function!', 'error');
		}
	},
	
	/** emit - Emit an event by name. {
		category:"method",
		arguments: [{
			type:"object",
			name:"eventName",
			desc:"The name of the event to listen for",
		}, {
			type:"multi",
			name:"args",
			desc:"The arguments to send to any listening methods. If you are sending multiple arguments, use an array containing each argument.",
		}],
	} **/
	emit: function (eventName, args) {
		// Check if the event has any listeners
		if (this._eventListeners[eventName]) {
			
			//console.log('Emitting ' + eventName + ' with ' + args);
			// Fire the listeners for this event
			var eventCount = this._eventListeners[eventName].length;
			
			// If there are some events, ensure that the args is ready to be used
			if (eventCount) {
				var finalArgs = [];
				if (typeof args != 'array') {
					if (typeof args == 'object' && args != null && args[0] != null) {
						for (var i in args) {
							finalArgs[i] = args[i];
						}
						/* Buggy code fixed with for each above
						for (var index = 0; index < args.length; index++) {
							finalArgs[index] = args[index];
						}
						*/
					} else {
						finalArgs = [args];
					}
				}
				
				// Loop and emit!
				var cancelFlag = false;
				
				while (eventCount--) {
					var tempEvt = this._eventListeners[eventName][eventCount];
					//console.log('Firing single-event ename: ' + eventName);
					
					// If the sendEventName flag is set, overwrite the arguments with the event name
					if (tempEvt.sendEventName) { finalArgs = [eventName]; }
					
					// Call the callback
					var retVal = tempEvt.call.apply(tempEvt.context || this, finalArgs);
					
					// If the retVal == 1 then store the cancel flag and return to the emitting method
					// TO-DO - Make this a constant that can be named
					if (retVal == 1) {
						// The receiver method asked us to send a cancel request back to the emitter
						cancelFlag = true;
					}
					
					// Check if we should now cancel the event
					if (tempEvt.oneShot) {
						// The event has a oneShot flag so since we have fired the event,
						// lets cancel the listener now
						this.cancelOn(eventName, tempEvt);
					}
				}
				
				if (cancelFlag) {
					return 1;
				}
				
			}
			
		}
	},
	
	/** cancelOn - Remove a listener. {
		category:"method",
		desc:"Remove a listener using the original arguments that were passed to the 'on' method to set the listener up",
		return:{
			type:"bool",
			desc:"Returns true if successful and false otherwise."
		},
		arguments: [{
			type:"object",
			name:"evtListener",
			desc:"The name of the event you originally registered to listen for",
		}, {
			type:"object",
			name:"call",
			desc:"The event listener object to cancel.",
		}],
	} **/
	cancelOn: function (eventName, evtListener) {
		if (this._eventListeners[eventName]) {
			// Find this listener in the list
			var evtListIndex = this._eventListeners[eventName].indexOf(evtListener);
			if (evtListIndex > -1) {
				// Remove the listener from the event listender list
				this._eventListeners[eventName].splice(evtListIndex, 1);
				return true;
			}
			this.log('Failed to cancel event listener for event named "' + eventName + '" !', 'info', evtListener);
		} else {
			this.log('Failed to cancel event listener!');
		}
		
		return false;
	},
	
	// Add a wrapper to continue support for the old way of handling events but warn each time it's used!
	events: {
		on: function () { var stack = new Error().stack; throw('+++++++++ ERROR +++++++++ [class].events.on() is depreciated in 0.2.2. Please use [class].on() instead!\n' + stack); process.exit(); },
		emit: function () { var stack = new Error().stack; throw('+++++++++ ERROR +++++++++ [class].events.emit() is depreciated in 0.2.2. Please use [class].emit() instead!\n' + stack); },
		cancelOn: function () { var stack = new Error().stack; throw('+++++++++ ERROR +++++++++ [class].events.cancelOn() is depreciated in 0.2.2. Please use [class].cancelOn() instead!\n' + stack); },
	},
	
});