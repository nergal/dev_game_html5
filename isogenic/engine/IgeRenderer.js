/** IgeRenderer - The render management class. {
	engine_ver:"0.1.0",
	category:"class",
} **/
IgeRenderer = new IgeClass({
	
	/** engine -  A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** entities - A reference to this.engine.entities for fast access. {
		category:"property",
		type:"object",
		instanceOf:"IgeEntities",
	} **/
	entities: null,
	
	/** viewports - A reference to this.engine.viewports for fast access. {
		category:"property",
		type:"object",
		instanceOf:"IgeViewports",
	} **/
	viewports: null,
	
	/** maps - A reference to this.engine.maps for fast access. {
		category:"property",
		type:"object",
		instanceOf:"IgeMaps",
	} **/
	maps: null,
	
	/** mapsById - A reference to this.engine.maps.byId for fast access. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	mapsById: null,
	
	/** byIndex - An array with an integer index that allows you to access each
	registered renderer class. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** tileToScreen - An array containing references to methods that can return
	co-ordinate conversions. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	tileToScreen: null,
	
	/** screenToTile - An array containing references to methods that can return
	co-ordinate conversions. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	screenToTile: null,
	
	/** renderCount - Stores the current tick's render count (number of draws to
	the screen). {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	renderCount: 0,
	
	/** statsOn - Determines if stats on the drawing methods are recorded for debug
	use. {
		category:"property",
		type:"bool",
	} **/
	statsOn: false,
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeRenderer';
		
		this.engine = engine;
		
		// Get references to objects that we need to access regularly
		this.screens = this.engine.screens;
		this.entities = this.engine.entities;
		this.viewports = this.engine.viewports;
		this.maps = this.engine.maps;
		this.mapsById = this.engine.maps.byId;
		this.dirtyRects = this.engine.dirtyRects;
		
		this.byIndex = [];
		
		this.tileToScreen = {};
		this.screenToTile = {};
		
		this.renderCount = 0;
		this.statsOn = true;
		
		// Define the isogenic renderers
		if (!this.engine.isServer) {
			this.byIndex.push(new IgeCanvas(this.engine));
			this.byIndex.push(new IgeHtml(this.engine));
		}
		
		// Define the co-ordinate functions
		this.mapToScreen = [this.mapToScreen2d, this.mapToScreenIso];
		this.viewToScreen = [this.viewToScreen2d, this.viewToScreenIso];
		this.screenToMap = [this.screenToMap2d, this.screenToMapIso];
		
	},
	
	/** viewToScreenIso - Returns the screen co-ordinates of the center of the specified tile x and y based
	upon the viewport's settings. This method is for isometric maps. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y.",
		},
		arguments: [{
			type:"integer",
			name:"mapX",
			desc:"The x co-ordinate of the map position.",
		}, {
			type:"integer",
			name:"mapY",
			desc:"The y co-ordinate of the map position.",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating the return value.",
		}, {
			type:"integer",
			name:"cornerIndex",
			desc:"If set, will return the position of a corner of the map tile rather than its center. Tile corner indexes are defined clockwise with top-left (in 2d) or top-middle (in iso) as TILE_CORNER_1.",
			flags:"optional",
		}],
	} **/
	viewToScreenIso: function (mapX, mapY, viewport, cornerIndex) {
		var nudgeX = 0, nudgeY = 0; // Default nudge values mean the center of the tile is returned
		
		// Check if we've been asked for a different point (a corner instead of the tile center)
		if (cornerIndex != null) {
			if (cornerIndex == TILE_CORNER_1) {
				nudgeY = -viewport.$local.$tileHeight2;
			}
			if (cornerIndex == TILE_CORNER_2) {
				nudgeX = viewport.$local.$tileWidth2;
			}
			if (cornerIndex == TILE_CORNER_3) {
				nudgeY = viewport.$local.$tileHeight2;
			}
			if (cornerIndex == TILE_CORNER_4) {
				nudgeX = -viewport.$local.$tileWidth2;
			}
		}
		return [((mapX - mapY) * viewport.$local.$tileWidth2) + viewport.viewport_anchor_point[0] + viewport.$viewportAdjustX + nudgeX, ((mapX + mapY) * viewport.$local.$tileHeight2) + viewport.viewport_anchor_point[1] + viewport.$viewportAdjustY + nudgeY];
	},
	
	/** mapToScreenIso - Converts map coordinates to screen coordinates. This method is for isometric maps. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"mapX",
			desc:"The x co-ordinate of the map position.",
		}, {
			type:"integer",
			name:"mapY",
			desc:"The y co-ordinate of the map position.",
		}, {
			type:"integer",
			name:"tileWidth",
			desc:"The width in pixels of a map tile.",
		}, {
			type:"integer",
			name:"tileHeight",
			desc:"The height in pixels of a map tile.",
		}, {
			type:"integer",
			name:"xNudge",
			desc:"Number of pixels to offset the final x co-ordinate by.",
		}, {
			type:"integer",
			name:"yNudge",
			desc:"Number of pixels to offset the final y co-ordinate by.",
		}],
	} **/
	mapToScreenIso: function (mapX, mapY, tileWidth, tileHeight, xNudge, yNudge) {
		// Return x and y as an array based upon tile size
		return [((mapX - mapY) * tileWidth / 2) + xNudge, ((mapX + mapY) * tileHeight / 2) + yNudge];
	},
	
	/** viewToScreen2d - Returns the screen co-ordinates of the center of the specified tile x and y based
	upon the viewport's settings. This method is for 2d maps. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"mapX",
			desc:"The x co-ordinate of the map position.",
		}, {
			type:"integer",
			name:"mapY",
			desc:"The y co-ordinate of the map position.",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating the return value.",
		}, {
			type:"integer",
			name:"cornerIndex",
			desc:"If set, will return the position of a corner of the map tile rather than its center. Tile corner indexes are defined clockwise with top-left (in 2d) or top-middle (in iso) as TILE_CORNER_1.",
			flags:"optional",
		}],
	} **/
	viewToScreen2d: function (mapX, mapY, viewport, cornerIndex) {
		var nudgeX = viewport.$local.$tileWidth2, nudgeY = viewport.$local.$tileHeight2; // Default nudge values mean the center of the tile is returned
		
		// Check if we've been asked for a different point (a corner instead of the tile center)
		if (cornerIndex != null) {
			if (cornerIndex == TILE_CORNER_1) {
				nudgeX += -viewport.$local.$tileWidth2;
				nudgeY += -viewport.$local.$tileHeight2;
			}
			if (cornerIndex == TILE_CORNER_2) {
				nudgeX += viewport.$local.$tileWidth2;
				nudgeY += -viewport.$local.$tileHeight2;
			}
			if (cornerIndex == TILE_CORNER_3) {
				nudgeX += viewport.$local.$tileWidth2;
				nudgeY += viewport.$local.$tileHeight2;
			}
			if (cornerIndex == TILE_CORNER_4) {
				nudgeX += -viewport.$local.$tileWidth2;
				nudgeY += viewport.$local.$tileHeight2;
			}
		}
		return [(mapX * viewport.$local.$tileWidth) + viewport.viewport_anchor_point[0] + viewport.$viewportAdjustX + nudgeX, (mapY * viewport.$local.$tileHeight) + viewport.viewport_anchor_point[1] + viewport.$viewportAdjustY + nudgeY];
	},
	
	/** mapToScreen2d - Converts map coordinates to screen coordinates. This method is for 2d maps. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"mapX",
			desc:"The x co-ordinate of the map position.",
		}, {
			type:"integer",
			name:"mapY",
			desc:"The y co-ordinate of the map position.",
		}, {
			type:"integer",
			name:"tileWidth",
			desc:"The width in pixels of a map tile.",
		}, {
			type:"integer",
			name:"tileHeight",
			desc:"The height in pixels of a map tile.",
		}, {
			type:"integer",
			name:"xNudge",
			desc:"Number of pixels to offset the final x co-ordinate by.",
		}, {
			type:"integer",
			name:"yNudge",
			desc:"Number of pixels to offset the final y co-ordinate by.",
		}],
	} **/
	mapToScreen2d: function (mapX, mapY, tileWidth, tileHeight, xNudge, yNudge) {
		return [(mapX * tileWidth) + xNudge, (mapY * tileHeight) + yNudge];
	},
	
	/** screenToMapIso - Returns the map co-ordinates of the tile that the specified x and y
	screen co-ordinates are intersecting. This method is for iso maps. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the screen position.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the screen position.",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating the return value.",
		}],
	} **/
	screenToMapIso: function (x, y, viewport) {
		var vpLocal = viewport.$local;
		var camera = vpLocal.$camera;
		var camSqr = (camera.camera_scale * camera.camera_scale);
		
		x += camera.camera_x * camSqr;
		y += camera.camera_y * camSqr;
		
		if (vpLocal.pan_panning) {
			x += (vpLocal.pan_change[0]);
			y += (vpLocal.pan_change[1]);
		}
		
		// Add our anchor point to the mouse cords so we have world cords
		x -= viewport.viewport_anchor_point[0] + viewport.$local.$width2 + (viewport.$local.$tileWidth2 * camera.camera_scale);
		y -= (viewport.viewport_anchor_point[1] + viewport.$local.$height2) - (viewport.$local.$tileHeight * camera.camera_scale);
		
		var tempX = 0, tempY = 0, tileX = 0, tileY = 0;
		
		tempX = x / (viewport.$local.$tileWidth2 * camera.camera_scale);
		tempY = y / (viewport.$local.$tileHeight2 * camera.camera_scale);
		
		tileY = Math.floor((tempX - tempY) / 2);
		tileX = Math.floor((tempX + tempY) / 2);
		
		tileY = -tileY;
		
		tileY--; tileY--;
		
		return [tileX, tileY, x + viewport.$local.$tileWidth2, y - viewport.$local.$tileHeight2];
		
	},
	
	/** screenToMap2d - Returns the map co-ordinates of the tile that the specified x and y
	screen co-ordinates are intersecting. This method is for 2d maps. {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the screen position.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the screen position.",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating the return value.",
		}],
	} **/
	screenToMap2d: function (x, y, viewport) {
		x += viewport.$local.$camera.camera_x;
		y += viewport.$local.$camera.camera_y;
		return [Math.floor((x - viewport.$local.$width2) / viewport.$local.$tileWidth), Math.floor((y - viewport.$local.$height2) / viewport.$local.$tileHeight)];
	},
	
	/** screen2dToScreenIso - Converts screen coordinates (2d) to screen coordinates (isometric). {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"screenX",
			desc:"The x co-ordinate of the screen position.",
		}, {
			type:"integer",
			name:"screenY",
			desc:"The y co-ordinate of the screen position.",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating the return value.",
		}],
	} **/	
	screen2dToScreenIso: function (screenX, screenY, viewport) {
		// Return x and y as an array
		var sX = (screenX / 2) - (screenY / 2);
		var sY = ((screenX / 2) + (screenY / 2)) / 2;
		
		sX += (viewport.$local.$width2);
		sY += (viewport.$local.$height2) - viewport.$local.$map.map_tilesize4;
		
		return [sX, sY];
	},
	
	/** screenIsoToScreen2d - Converts screen coordinates (iso) to screen coordinates (2d). {
		category:"method",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"screenX",
			desc:"The x co-ordinate of the screen position.",
		}, {
			type:"integer",
			name:"screenY",
			desc:"The y co-ordinate of the screen position.",
		}, {
			type:"object",
			name:"viewport",
			desc:"The viewport to use when calculating the return value.",
		}],
	} **/	
	screenIsoToScreen2d: function (screenX, screenY, viewport) {
		// Add our anchor point to the mouse cords so we have world cords
		screenX += viewport.$local.$camera.camera_x;
		screenY += viewport.$local.$camera.camera_y;
		
		var sY = ((2 * screenY) - screenX);
		var sX = ((screenX / 2) + screenY) * 2;
		
		sX += viewport.$local.$map.map_tilesize2;
		sY += viewport.$local.$map.map_tilesize2;
		
		return [sX, sY];
	},
	
	/** scoreDepth - Depreciated. {
		category:"method",
		return: {
			type:"integer",
			desc:"Returns a depth score as an integer value. ",
		},
		arguments: [{
			type:"integer",
			name:"a",
			desc:"Depreciated.",
		}, {
			type:"integer",
			name:"b",
			desc:"Depreciated",
		}],
	} **/	
	scoreDepth: function (a, b) {
		//console.log("Score: ", a, b);
		return a[1] - b[1];
	},
	
	/** tileCordsToDepth - Calculate the depth as an integer value by the passed tile co-ordinates. {
		category:"method",
		engine_ver:"0.1.0",
		return: {
			type:"integer",
			desc:"Returns a depth score as an integer value. ",
		},
		arguments: [{
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the tile position.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the tile position.",
		}, {
			type:"integer",
			name:"z",
			desc:"The z co-ordinate of the tile position.",
		}, {
			type:"integer",
			name:"w",
			desc:"The width in tiles.",
		}, {
			type:"integer",
			name:"h",
			desc:"The height in tiles.",
		}, {
			type:"integer",
			name:"orient",
			desc:"The orientation of the tile map (always use '0' for this value at the moment).",
		}],
	} **/	
	tileCordsToDepth: function (x, y, z, w, h, orient) {
		var depth = 0;
		
		var finalX = (x + Math.round(w / 3, 2)) * 1000 + (w - 1) * 3;
		var finalY = (y + Math.round(h / 3, 2)) * 1000 + (h - 1) * 3;
		
		// 0, 0 is at center, top
		if (orient == 0) {
			
			depth = finalX + finalY + z;
			return depth / 10000;
			
		}
		
		// 0, 0 is at right, center
		if (orient == 1) {
			
			depth = (finalX - finalY) + z;
			return depth / 10000;
			
		}
		
		// 0, 0 is at center, bottom
		if (orient == 2) {
			
			depth = (0 - (finalX + finalY)) + z;
			return depth / 10000;
			
		}
		
		// 0, 0 is at left, center
		if (orient == 3) {
			
			depth = (finalY - finalX) + z;
			return depth / 10000;
			
		}
	},
	
	/** screenCordsToDepth - Calculate the depth as an integer value by the passed screen co-ordinates. {
		category:"method",
		engine_ver:"0.1.0",
		return: {
			type:"integer",
			desc:"Returns a depth score as an integer value. ",
		},
		arguments: [{
			type:"integer",
			name:"x",
			desc:"The x co-ordinate of the screen position.",
		}, {
			type:"integer",
			name:"y",
			desc:"The y co-ordinate of the screen position.",
		}, {
			type:"integer",
			name:"orient",
			desc:"The orientation of the tile map (always use '0' for this value at the moment).",
		}],
	} **/
	screenCordsToDepth: function (x, y, orient) {
		var depth = 0;
		var z = 0;
		
		// 0, 0 is at center, top
		if (orient == 0) {
			
			depth = x + y + z;
			return depth;
			
		}
		
		// 0, 0 is at right, center
		if (orient == 1) {
			
			depth = (x - y) + z;
			return depth;
			
		}
		
		// 0, 0 is at center, bottom
		if (orient == 2) {
			
			depth = (0 - (x + y)) + z;
			return depth;
			
		}
		
		// 0, 0 is at left, center
		if (orient == 3) {
			
			depth = (y - x) + z;
			return depth;
			
		}
	},
	
	tileWalkData: {
		n:	{ x:-1,	y:-1 },
		ne:	{ x:0,	y:-1 },
		e:	{ x:1,	y:-1 },
		se:	{ x:1,	y:0 },
		s:	{ x:1,	y:1 },
		sw:	{ x:0,	y:1 },
		w:	{ x:-1,	y:1 },
		nw:	{ x:-1,	y:0 },
	},
	
	/** tileWalk - Calculate the destination tile co-ordinates given a starting tile, distance
	and direction. {
		category:"method",
		engine_ver:"0.1.0",
		return: {
			type:"array",
			desc:"Returns an array containing co-ordinates defined as [0] = x, [1] = y. ",
		},
		arguments: [{
			type:"integer",
			name:"startX",
			desc:"The starting tile x co-ordinate.",
		}, {
			type:"integer",
			name:"startY",
			desc:"The starting tile y co-ordinate.",
		}, {
			type:"string",
			name:"direction",
			desc:"The direction to 'walk' in as a compass direction such as 'n' for north or 'sw' for south-west.",
		}, {
			type:"integer",
			name:"distance",
			desc:"The distance in tiles to 'walk'.",
		}],
	} **/
	tileWalk: function (startX, startY, direction, distance) {
		if (distance == null) { distance = 1; }
		var walkData = this.tileWalkData[direction];
		return [startX += (walkData.x * distance), startY += (walkData.y * distance)];
	},
	
	/** render - Renders all the active viewports and map data. {
		category:"method",
		engine_ver:"0.1.0",
		arguments: [{
			type:"integer",
			name:"tickTime",
			desc:"The current engine tick time.",
		}],
	} **/
	render: function (tickTime) {
		
		var viewport = null;
		var viewportCount = 0;
		var viewportsByScreenId = null;
		var layerCount = 0;
		var layer = null;
		var mapCleanCheck = [];
		var mapCleanList = [];
		var layerCleanList = [];
		var gridLayer = null;
		
		// Get the screen render list
		var curScreen = null;
		var rList = this.screens.renderList;
		var rListCount = rList.length;
		
		// Loop the render list
		while (rListCount--) {
			curScreen = rList[rListCount];
			
			// Check if a screen is actually currently selected
			if (curScreen != null) {
				
				// Grab all the viewports assigned to this screen
				viewportsByScreenId = this.viewports.byScreenId[curScreen.screen_id];
				
				// Check if we have any viewports for this screen!
				if (viewportsByScreenId != null) {
					
					viewportCount = viewportsByScreenId.length;
					
					// Select each viewport
					while (viewportCount--) {
						
						viewport = viewportsByScreenId[viewportCount];
						
						// Only render this viewport if it is visible
						if (!viewport.viewport_hide) {
							
							// Does the viewport have a map assigned? (Cannot render anything without a map!)
							if (viewport.map_id) {
								
								var map = viewport.$local.$map;
								
								// Is the map set to render at the moment?
								if (map.map_render) {
									
									// Create a variable to receive a count of drawn entities
									var rc = 0;
									
									// Check if the viewport needs a complete redraw
									if (viewport.$local.$viewport_dirty) {
										
										// Complete redraw needed so ignore dirty rect system and do redraw by
										// looping through each viewport layer and rendering it
										layerCount = viewport.drawLayers.length;
										while (layerCount--) {
											// Check the layer type and call the render function of the correct renderer
											layer = viewport.drawLayers[layerCount];
											
											// Only render this layer if it has been marked as dirty!
											if (layer.$local.$layer_dirty) {
												// Render the layer
												rc += this.byIndex[layer.layer_type].renderFull(viewport, layerCount, tickTime);
												
												// Add the layer to the cleanup list
												layerCleanList[map.map_id] = layerCleanList[map.map_id] || [];
												layerCleanList[map.map_id][layerCount] = layer;
											}
											
											if (layer.layer_draw_grid) {
												gridLayer = layerCount;
											}											
										}
										
										// Mark the viewport as clean
										viewport.$local.$viewport_dirty = false;
										
										// Set the clean section move to nothing because we've done a complete redraw
										viewport.$local.cleanSectionMove = null;
										
									} else {
										
										if (map.map_use_dirty && map.map_dirty_grid && map.map_layers[3].layer_type == LAYER_TYPE_CANVAS) {
											var buffer = viewport.$local.$frontBuffer[3];
											var bufCtx = viewport.$local.$frontBufferCtx[3];
											bufCtx.clearRect(0, 0, buffer.width, buffer.height);
											this.engine.dirtyRects.drawGrid(viewport, 3);
										}
										
										// Process any clean section move first
										if (viewport.$local.cleanSectionMove && (viewport.$local.cleanSectionMove[0] || viewport.$local.cleanSectionMove[1])) {
											// We have a clean section move to perform - basically this will "move" the current image
											// data on each of the active layers (not including background, ui or other static layers)
											// by the amount in the value array. This is a really cool way to avoid redrawing large
											// amounts of entities and large parts of a viewport after a panning operation or camera
											// lookAt / lookBy call.
											
											// Work out the area that will be copied (moved)
											var sourceAreaX = 0;
											var sourceAreaY = 0;
											var destAreaX = 0;
											var destAreaY = 0;
											var moveX = viewport.$local.cleanSectionMove[0];
											var moveY = viewport.$local.cleanSectionMove[1];
											var areaWidth = viewport.panLayer.width - Math.abs(moveX);
											var areaHeight = viewport.panLayer.height - Math.abs(moveY);
											
											// Adjust the areaX and areaY based upon the direction of clean copy
											if (moveX > 0) { sourceAreaX = Math.abs(moveX); }
											if (moveY > 0) { sourceAreaY = Math.abs(moveY); }
											if (moveX < 0) { destAreaX = Math.abs(moveX); }
											if (moveY < 0) { destAreaY = Math.abs(moveY); }
											
											// Now move the image data by looping the viewport layers and checking the layer type
											var backBuffer = viewport.$local.$backBuffer;
											var backBufferCtx = viewport.$local.$backBufferCtx;
											var sectionLayers = viewport.$local.$frontBuffer;
											var sectionLayersCtx = viewport.$local.$frontBufferCtx;
											
											var mapLayer = null;
											var layers = map.map_layers;
											layerCount = layers.length;
											
											while (layerCount--) {
												mapLayer = map.map_layers[layerCount];
												
												// Check that the layer is canvas-based
												if (mapLayer.layer_type == LAYER_TYPE_CANVAS) {
													
													if (mapLayer.layer_entity_types == LAYER_TILES || mapLayer.layer_entity_types == LAYER_OBJECTS || mapLayer.layer_entity_types == LAYER_SPRITES) {
														// The layer is not associated with static image data so move it!
														var layer = sectionLayers[layerCount];
														var layerCtx = sectionLayersCtx[layerCount];
														
														// Clear backbuffer area we are going to use
														backBufferCtx.clearRect(sourceAreaX, sourceAreaY, areaWidth, areaHeight);
														
														// Draw the image data to the backbuffer
														backBufferCtx.drawImage(layer, sourceAreaX, sourceAreaY, areaWidth, areaHeight, sourceAreaX, sourceAreaY, areaWidth, areaHeight);
														rc++;
														// Clear frontbuffer area we are going to use
														layerCtx.clearRect(destAreaX, destAreaY, areaWidth, areaHeight);
														//var imgData = layerCtx.getImageData(sourceAreaX, sourceAreaY, areaWidth, areaHeight);
														
														// Copy image data from the backbuffer to the frontbuffer
														layerCtx.drawImage(backBuffer, sourceAreaX, sourceAreaY, areaWidth, areaHeight, destAreaX, destAreaY, areaWidth, areaHeight);
														//layerCtx.putImageData(imgData, destAreaX, destAreaY);
														rc++;
													}
													
												}
											}
											viewport.$local.cleanSectionMove = null;
										}
										
										// Now check if this viewport has any dirty sections - these are NOT dirty rectangles!
										var dsArray = viewport.$local.dirtySections;
										if (dsArray && dsArray.length) {
											
											// Check if the map is using dirty rectangles and if not, just ask for a full redraw
											if (map.map_use_dirty) {
												// We have dirty sections! Calculate the dirty rects they intersect!
												var centerX = viewport.$viewportAdjustX;
												var centerY = viewport.$viewportAdjustY;
												var dr = map.$local.$dirtyRects;
												var dsCount = dsArray.length;
												var tempDirtyList = [];
												
												while (dsCount--) {
													var rect = dsArray[dsCount];
													
													// Calculate which dirty rects the corners of the rect intersect
													// Top-left
													var tile1 = this.engine.dirtyRects.pointInTile(rect[0] - centerX, rect[1] - centerY, dr);
													// Top-right
													var tile2 = this.engine.dirtyRects.pointInTile(rect[0] - centerX + rect[2], rect[1] - centerY, dr);
													// Bottom-left
													var tile3 = this.engine.dirtyRects.pointInTile(rect[0] - centerX, rect[1] + rect[3] - centerY, dr);
													// Bottom-right
													var tile4 = this.engine.dirtyRects.pointInTile(rect[0] - centerX + rect[2], rect[1] + rect[3] - centerY, dr);
													
													// Now that we have the corners, loop through the tiles and mark them dirty
													for (var dirtyTileX = tile1[0]; dirtyTileX <= tile2[0]; dirtyTileX++) {
														
														for (var dirtyTileY = tile1[1]; dirtyTileY <= tile3[1]; dirtyTileY++) {
															
															//this.markDirtyByTile(dr, dirtyTileX, dirtyTileY, layerIndex);
															tempDirtyList.push([dirtyTileX, dirtyTileY]);
															
														}
														
													}
												}
												
												// Now if there were any dirty rects, draw them now, before the main dr list is processed below
												var layers = map.map_layers;
												layerCount = layers.length;
												layerCount--;
												while (layerCount--) {
													rc += this.byIndex[layers[layerCount].layer_type].renderRects(viewport, layerCount, tickTime, map, tempDirtyList);
													if (layers[layerCount].layer_draw_grid) {
														gridLayer = layerCount;
													}
												}
												
											} else {
												// The map is not using dirty rectangles so request a full redraw
												var layers = map.map_layers;
												layerCount = layers.length;
												layerCount--;
												while (layerCount--) {
													rc += this.byIndex[layers[layerCount].layer_type].renderFull(viewport, layerCount, tickTime);
													if (layers[layerCount].layer_draw_grid) {
														gridLayer = layerCount;
													}
												}
											}
											
											// Now clear the sections
											this.engine.viewports.clearDirtySections(viewport);
										}
										
										// The viewport does not need a full redraw so check for dirty rectangles
										// on the map it is looking at to see if we need to redraw any of those!
										var layers = map.map_layers;
										layerCount = layers.length;
										layerCount--;
										while (layerCount--) {
											// Check if the map is dirty
											if (this.dirtyRects.isMapDirty(map, layerCount)) {
												// Map contains dirty rectangles so redraw them on the viewport
												rc += this.byIndex[layers[layerCount].layer_type].renderRects(viewport, layerCount, tickTime, map, map.$local.$dirtyRects.layer[layerCount].dirty);
											}
											if (layers[layerCount].layer_draw_grid) {
												gridLayer = layerCount;
											}
										}
										
									}
									
									// Add map to clean list
									if (map.map_use_dirty) { mapCleanList.push(map); }
									
									//if (this.statsOn) {
										this.renderCount += rc;
									//}
									
									if (gridLayer != null) {
										this.drawMapGrid(viewport, map, gridLayer);
									}
									
								}
								
							}
							
						}
						
					}
					
				}
				
				// Process the layer clean list
				if (layerCleanList.length) {
					for (var i in layerCleanList) {
						var lclItemArray = layerCleanList[i];
						for (var k in lclItemArray) {
							lclItemArray[k].$local.$layer_dirty = false;
						}
					}
				}
				
				// If we processed any map dirty rects
				if (mapCleanList.length) {
					
					// Loop through the maps that we processed
					var count = mapCleanList.length;
					while (count--) {
						// Mark the map as clean (remove all dirty rects)!
						this.dirtyRects.cleanMap(mapCleanList[count]);
					}
					
				}
				
			}
			
		}
		
	},
	
	/** drawMapGrid - Draws a grid based upon the tile-map settings of the passed map and viewport,
	on the given layer index. Only works on canvas-based layers. This should only be used for debug
	as it is not performance-tuned and will affect your game's rendering performance. {
		category:"method",
		engine_ver:"0.1.0",
		arguments: [{
			type:"object",
			name:"viewport",
			desc:"The viewport on which to draw the tile grid.",
		}, {
			type:"object",
			name:"map",
			desc:"The map whose data to use when drawing the tile grid.",
		}, {
			type:"integer",
			name:"layerIndex",
			desc:"The index of the map layer on which to draw the tile grid.",
		}],
	} **/
	drawMapGrid: function (viewport, map, layerIndex) {
		// Check if the layer has show grid enabled, if so draw the map's tile grid on this layer now
		var frontBuffer = viewport.$local.$frontBuffer[layerIndex];
		var frontCtx = viewport.$local.$frontBufferCtx[layerIndex];
		
		if (frontCtx != null) {
			
			var dtr = viewport.viewport_tile_ratio;
			var mapTileSize = map.map_tilesize;
			
			frontCtx.strokeStyle = '#00ff00';
			frontCtx.lineWidth = 0.2;
			
			var gridSize = viewport.viewport_container.width;
			if (viewport.viewport_container.height > gridSize) { gridSize = viewport.viewport_container.height; }
			
			if (map.map_render_mode == MAP_RENDER_MODE_2D) { gridSize = gridSize / 2; }
			
			var startTileX = -Math.floor((gridSize) / map.map_tilesize);
			var startTileY = startTileX;
			
			var endTileX = Math.abs(startTileX);
			var endTileY = endTileX;
			
			var tileCords1 = this.engine.renderer.viewToScreen[map.map_render_mode](startTileX, startTileY, viewport, TILE_CORNER_1);
			var tileCords2 = this.engine.renderer.viewToScreen[map.map_render_mode](endTileX, startTileY, viewport, TILE_CORNER_2);
			var tileCords4 = this.engine.renderer.viewToScreen[map.map_render_mode](startTileX, endTileY, viewport, TILE_CORNER_4);
			
			var tileWidth = viewport.$local.$tileWidth * dtr;
			var tileHeight = viewport.$local.$tileHeight * dtr;
			
			if (map.map_render_mode == MAP_RENDER_MODE_2D) {
				for (var x = 0; x <= (endTileX * 2) + 1; x++) {
					frontCtx.beginPath();
					frontCtx.moveTo(tileCords1[0], tileCords1[1] + (tileHeight * x));
					frontCtx.lineTo(tileCords2[0], tileCords2[1] + (tileHeight * x));
					frontCtx.stroke();
					
					frontCtx.beginPath();
					frontCtx.moveTo(tileCords1[0] + (tileWidth * x), tileCords1[1]);
					frontCtx.lineTo(tileCords4[0] + (tileWidth * x), tileCords4[1]);
					frontCtx.stroke();
				}
			}
			
			if (map.map_render_mode == MAP_RENDER_MODE_ISOMETRIC) {
				for (var x = 0; x <= (endTileX * 2) + 1; x++) {
					frontCtx.beginPath();
					frontCtx.moveTo(tileCords1[0] - (tileWidth * x), tileCords1[1] + (tileHeight * x));
					frontCtx.lineTo(tileCords2[0] - (tileWidth * x), tileCords2[1] + (tileHeight * x));
					frontCtx.stroke();
					
					frontCtx.beginPath();
					frontCtx.moveTo(tileCords1[0] + (tileWidth * x), tileCords1[1] + (tileHeight * x));
					frontCtx.lineTo(tileCords4[0] + (tileWidth * x), tileCords4[1] + (tileHeight * x));
					frontCtx.stroke();
				}
			}
			
		}
	},
	
});