/** IgePhysics - Box2D physics library for Isogenic Engine. {
	urls:[
		"http://www.box2dflash.org/docs/2.1a/reference/",
		"http://code.google.com/p/box2dweb/",
	],
} **/
IgePhysics = new IgeClass({
	
	engine: null,
	box2d: null,
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Variables
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	_gravity: null,
	_doSleep: null,
	_ptm: null,
	
	_world: null,
	
	_ready: null,
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Constructor
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	init: function (engine) {
		this._className = 'IgePhysics';
		
		this.engine = engine;
		this.engine.physics = this;
		
		this.box2d = {};
		this._ready = false;
		
		// Register a requirement check against this module's state method
		this.engine.registerRequirement(this.bind(this.state));
		
		// Load the box2d library
		/* CEXCLUDE */
		if (this.engine.isServer) {
			this.box2d = require(this.engine.config.dir_engine + '/modules/IgePhysics/lib_box2d_2.1a');
		}
		/* CEXCLUDE */
		if (!this.engine.isServer) {
			igeBootstrap.require('../engine/modules/IgePhysics/lib_box2d_2.1a', 'physics', this.bind(function () {
				this.log('Loaded all box2d script data!');
				
				this.box2d.b2BoundValues = b2BoundValues;
				this.box2d.b2Math = b2Math;
				this.box2d.b2DistanceOutput = b2DistanceOutput;
				this.box2d.b2Mat33 = b2Mat33;
				this.box2d.b2ContactPoint = b2ContactPoint;
				this.box2d.b2PairManager = b2PairManager;
				this.box2d.b2PositionSolverManifold = b2PositionSolverManifold;
				this.box2d.b2OBB = b2OBB;
				this.box2d.b2CircleContact = b2CircleContact;
				this.box2d.b2PulleyJoint = b2PulleyJoint;
				this.box2d.b2Pair = b2Pair;
				this.box2d.b2TimeStep = b2TimeStep;
				this.box2d.b2FixtureDef = b2FixtureDef;
				this.box2d.b2World = b2World;
				this.box2d.b2PrismaticJoint = b2PrismaticJoint;
				this.box2d.b2Controller = b2Controller;
				this.box2d.b2ContactID = b2ContactID;
				this.box2d.b2RevoluteJoint = b2RevoluteJoint;
				this.box2d.b2JointDef = b2JointDef;
				this.box2d.b2Transform = b2Transform;
				this.box2d.b2GravityController = b2GravityController;
				this.box2d.b2EdgeAndCircleContact = b2EdgeAndCircleContact;
				this.box2d.b2EdgeShape = b2EdgeShape;
				this.box2d.b2BuoyancyController = b2BuoyancyController;
				this.box2d.b2LineJointDef = b2LineJointDef;
				this.box2d.b2Contact = b2Contact;
				this.box2d.b2DistanceJoint = b2DistanceJoint;
				this.box2d.b2Body = b2Body;
				this.box2d.b2DestructionListener = b2DestructionListener;
				this.box2d.b2PulleyJointDef = b2PulleyJointDef;
				this.box2d.b2ContactEdge = b2ContactEdge;
				this.box2d.b2ContactConstraint = b2ContactConstraint;
				this.box2d.b2ContactImpulse = b2ContactImpulse;
				this.box2d.b2DistanceJointDef = b2DistanceJointDef;
				this.box2d.b2ContactResult = b2ContactResult;
				this.box2d.b2EdgeChainDef = b2EdgeChainDef;
				this.box2d.b2Vec2 = b2Vec2;
				this.box2d.b2Vec3 = b2Vec3;
				this.box2d.b2DistanceProxy = b2DistanceProxy;
				this.box2d.b2FrictionJointDef = b2FrictionJointDef;
				this.box2d.b2PolygonContact = b2PolygonContact;
				this.box2d.b2TensorDampingController = b2TensorDampingController;
				this.box2d.b2ContactFactory = b2ContactFactory;
				this.box2d.b2WeldJointDef = b2WeldJointDef;
				this.box2d.b2ConstantAccelController = b2ConstantAccelController;
				this.box2d.b2GearJointDef = b2GearJointDef;
				this.box2d.ClipVertex = ClipVertex;
				this.box2d.b2SeparationFunction = b2SeparationFunction;
				this.box2d.b2ManifoldPoint = b2ManifoldPoint;
				this.box2d.b2Color = b2Color;
				this.box2d.b2PolygonShape = b2PolygonShape;
				this.box2d.b2DynamicTreePair = b2DynamicTreePair;
				this.box2d.b2ContactConstraintPoint = b2ContactConstraintPoint;
				this.box2d.b2FrictionJoint = b2FrictionJoint;
				this.box2d.b2ContactFilter = b2ContactFilter;
				this.box2d.b2ControllerEdge = b2ControllerEdge;
				this.box2d.b2Distance = b2Distance;
				this.box2d.b2Fixture = b2Fixture;
				this.box2d.b2DynamicTreeNode = b2DynamicTreeNode;
				this.box2d.b2MouseJoint = b2MouseJoint;
				this.box2d.b2DistanceInput = b2DistanceInput;
				this.box2d.b2BodyDef = b2BodyDef;
				this.box2d.b2DynamicTreeBroadPhase = b2DynamicTreeBroadPhase;
				this.box2d.b2Settings = b2Settings;
				this.box2d.b2Proxy = b2Proxy;
				this.box2d.b2Point = b2Point;
				this.box2d.b2BroadPhase = b2BroadPhase;
				this.box2d.b2Manifold = b2Manifold;
				this.box2d.b2WorldManifold = b2WorldManifold;
				this.box2d.b2PrismaticJointDef = b2PrismaticJointDef;
				this.box2d.b2RayCastOutput = b2RayCastOutput;
				this.box2d.b2ConstantForceController = b2ConstantForceController;
				this.box2d.b2TimeOfImpact = b2TimeOfImpact;
				this.box2d.b2CircleShape = b2CircleShape;
				this.box2d.b2MassData = b2MassData;
				this.box2d.b2Joint = b2Joint;
				this.box2d.b2GearJoint = b2GearJoint;
				this.box2d.b2DynamicTree = b2DynamicTree;
				this.box2d.b2JointEdge = b2JointEdge;
				this.box2d.b2LineJoint = b2LineJoint;
				this.box2d.b2NullContact = b2NullContact;
				this.box2d.b2ContactListener = b2ContactListener;
				this.box2d.b2RayCastInput = b2RayCastInput;
				this.box2d.b2TOIInput = b2TOIInput;
				this.box2d.Features = Features;
				this.box2d.b2FilterData = b2FilterData;
				this.box2d.b2Island = b2Island;
				this.box2d.b2ContactManager = b2ContactManager;
				this.box2d.b2ContactSolver = b2ContactSolver;
				this.box2d.b2Simplex = b2Simplex;
				this.box2d.b2AABB = b2AABB;
				this.box2d.b2Jacobian = b2Jacobian;
				this.box2d.b2Bound = b2Bound;
				this.box2d.b2RevoluteJointDef = b2RevoluteJointDef;
				this.box2d.b2PolyAndEdgeContact = b2PolyAndEdgeContact;
				this.box2d.b2SimplexVertex = b2SimplexVertex;
				this.box2d.b2WeldJoint = b2WeldJoint;
				this.box2d.b2Collision = b2Collision;
				this.box2d.b2Mat22 = b2Mat22;
				this.box2d.b2SimplexCache = b2SimplexCache;
				this.box2d.b2PolyAndCircleContact = b2PolyAndCircleContact;
				this.box2d.b2MouseJointDef = b2MouseJointDef;
				this.box2d.b2Shape = b2Shape;
				this.box2d.b2Segment = b2Segment;
				this.box2d.b2ContactRegister = b2ContactRegister;
				this.box2d.b2DebugDraw = b2DebugDraw;
				this.box2d.b2Sweep = b2Sweep;
				
				this._ready = true;
			}));
		}
		
		// Set the default pixels to metres ratio
		this._ptm = 30; // In Box2D, 30px = 1 metre
	},
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** engineHooks - Framework method. {
		category:"method",
	} **/
	engineHooks: function () { return true; },
	
	/** networkCommands - Framework method. {
		category:"method",
	} **/
	networkCommands: function () { return true; },
	
	/** networkProperties - Framework method. {
		category:"method",
	} **/
	networkProperties: function () { return true; },
	
	/** modules - Framework method. {
		category:"method",
	} **/
	modules: function () { return true; },
	
	/** moduleHooks - Framework method. {
		category:"method",
	} **/
	moduleHooks: function () { return true; },
	
	/** ready - Called by the engine after all other module framework methods have been called. {
		category:"method",
	} **/
	ready: function () {
		this.log('Engine says we are ready to go!');
		return true;
	},
	
	/** state - Check the current state of the module.
	Returns false if we're still waiting on something. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the module is ready, false if not.",
		},
	} **/
	state: function () {
		return this._ready;
	},
	
	/* createWorld - Create the world for the Box2D simulation */
	createWorld: function () {
		// World params - gravity, allow sleep
		this.log('Creating world...');
		this.world = new this.box2d.b2World(this._gravity, this._doSleep);
		return this.world;
	},
	
	/* setBounds - Depreciated; Set the bounds that box2D will use for the world */
	/*setBounds: function (x1, y1, x2, y2) {
		this._bounds = new b2AABB(b2Vec2(x1, y1), b2Vec2(x2, y2));
	},*/
	
	setDebugCanvas: function (canvasId) {
		var debugDraw = new this.box2d.b2DebugDraw;
		debugDraw.SetSprite($('#' + canvasId)[0].getContext("2d"));		
	},
	
	/* setGravity - Set the gravity vector. */
	setGravity: function (x, y) {
		this._gravity = new this.box2d.b2Vec2(x, y);
	},
	
	setDoSleep: function (b) {
		this._doSleep = b;
	},
	
	setPtm: function (v) {
		this._ptm = v;
	},
	
	/* createShape - Creates a shape; shapes cannot be reused!
	b2Shape Reference: http://www.box2dflash.org/docs/2.1a/reference/Box2D/Collision/Shapes/b2Shape.html
	
	Viable parameters are:
		type (b2PolygonShape, b2CircleShape), params (object)
	*/
	createShape: function (type, params) {
		var newShape = type;
		for (param in params) { newShape[param] = params[param]; }
		
		return newShape;
	},
	
	/* createFixture - Creates a box2D fixture with the parameters object passed.
	b2FixtureDef Reference: http://www.box2dflash.org/docs/2.1a/reference/Box2D/Dynamics/b2FixtureDef.html
	
	Viable parameters are:
		density (float) - The density, usually in kg/m^2.
		filter (b2FilterData) - Contact filtering data.
		friction (float) - The friction coefficient, usually in the range [0,1].
		isSensor (boolean) - A sensor shape collects contact information but never generates a collision response.
		restitution (float) - The restitution (elasticity) usually in the range [0,1].
		shape (b2Shape) - The shape, this must be set.
		userData (object / array) - Use this to store application specific fixture data.
	*/
	createFixture: function (params, construct) {
		var newFixture = new this.b2FixtureDef;
		for (param in params) { newFixture[param] = params[param]; }
		
		return newFixture;
	},
	
	/* createBody - 
	
	Viable parameters are:
		type (object) - b2Body.b2_staticBody, b2Body.b2_dynamicBody, b2Body.b2_kinematicBody
	*/
	createBody: function (params) {
		var bodyDef = new this.b2BodyDef;
		
		if (params.angle) { params.angle *= Math.PI / 180; }
		for (param in params) { if (param != 'fixture') { bodyDef[param] = params[param]; } }
		
		return this.world.CreateBody(bodyDef).CreateFixture(params.fixture);
	},
	
	createEntity: function (entityParams) {
		
		// Define physics entity
		var bodyDef = new this.b2BodyDef;
		var newFix = new this.b2FixtureDef;
		var newShape = null;
		
		if (entityParams.fixtureDef.density) { newFix.density = entityParams.fixtureDef.density; }
		if (entityParams.fixtureDef.friction) { newFix.friction = entityParams.fixtureDef.friction; }
		if (entityParams.fixtureDef.restitution) { newFix.restitution = entityParams.fixtureDef.restitution; }
		if (entityParams.fixtureDef.filter) { newFix.filter = entityParams.fixtureDef.filter; }
		if (entityParams.fixtureDef.isSensor) { newFix.isSensor = entityParams.fixtureDef.isSensor; }
		if (entityParams.fixtureDef.userData) { newFix.userData = entityParams.fixtureDef.userData; }
		
		switch (entityParams.shapeDef.type)
		{
			
			case 0:
				newShape = new this.b2PolygonShape;
			break;
			
			case 1:
				newShape = new this.b2CircleShape;
			break;
			
		}
		
		if (entityParams.shapeDef.radius) { newShape.SetRadius(parseFloat((entityParams.shapeDef.radius / this._ptm).toFixed(2))); }
		if (entityParams.shapeDef.box) { newShape.SetAsBox(parseFloat((entityParams.shapeDef.box.width / this._ptm).toFixed(2)), parseFloat((entityParams.shapeDef.box.height / this._ptm).toFixed(2))); }
		if (entityParams.shapeDef.arr)
		{
			
			for (var i = 0; i < entityParams.shapeDef.array.length; i++)
			{
				
				entityParams.shapeDef.array[i] = parseFloat((entityParams.shapeDef.array[i] / this._ptm).toFixed(5));
				
			}
			
			newShape.SetAsArray(entityParams.shapeDef.array, entityParams.shapeDef.array.length);
			
		}
		
		newFix.shape = newShape;
		
		bodyDef.type = entityParams.type;
		
		if (entityParams.bodyDef.angle) { bodyDef.angle = parseFloat((entityParams.bodyDef.angle * Math.PI / 180).toFixed(2)); }
		
		bodyDef.position.x = parseFloat((entityParams.bodyDef.screenCords.x / this._ptm).toFixed(2)) + parseFloat((entityParams.shapeDef.width / this._ptm).toFixed(2));
		bodyDef.position.y = parseFloat((entityParams.bodyDef.screenCords.y / this._ptm).toFixed(2)) + parseFloat((entityParams.shapeDef.height / this._ptm).toFixed(2));;
		
		var finalBody = this.world.CreateBody(bodyDef);
		finalBody.CreateFixture(newFix);
		//finalBody.SetMassFromShapes();
		//finalBody.SetLinearDamping(0.2);
		
		return finalBody;
		
	},
	
	shapeSetBox: function (shape, w, h) {
		
		var fw = (w / this._ptm).toFixed(2);
		var fh = (h / this._ptm).toFixed(2);
		
		shape.SetAsBox(fw, fh);
		
	},
	
	/* getBodyAtMouse - Pick the body underneath the mouse position and return it */
	getBodyAtMouse: function (includeStatic) {
		
		/* TO-DO - Convert the remainder of this function from actionscript
		based to javascript and ensure that any references are sorted out! */
		
		real_x_mouse = (stage.mouseX) / this._ptm;
		real_y_mouse = (stage.mouseY) / this._ptm;
		
		mousePVec.Set(real_x_mouse, real_y_mouse);
		
		var aabb = new b2AABB();
		aabb.lowerBound.Set(real_x_mouse - 0.001, real_y_mouse - 0.001);
		aabb.upperBound.Set(real_x_mouse + 0.001, real_y_mouse + 0.001);
		
		var k_maxCount = 10;
		var shapes = []
		var count = m_world.Query(aabb, shapes, k_maxCount);
		var tempBody = null;
		
		for (var i = 0; i < count; i++)
		{
			
			if (shapes[i].m_body.IsStatic() == false || includeStatic)
			{
				
				var tShape = shapes[i];
				var inside = tShape.TestPoint(tShape.m_body.GetXForm(), mousePVec);
				
				if (inside)
				{
					
					tempBody = tShape.m_body;
					break;
					
				}
				
			}
			
		}
		
		return tempBody;
	},
	
	/* step - Step the simulation forward by the deltaTime */
	step: function (deltaTime) {
		// Call the world step; frame-rate, velocity iterations, position iterations 
		this.world.Step(1 / 60, 10, 10);
		
		// Redraw the debug data
		//this.world.DrawDebugData();
		
		// Clear forces because we have ended our physics simulation frame
		//this.world.ClearForces();
	},
	
});