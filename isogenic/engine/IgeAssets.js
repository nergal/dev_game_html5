/** IgeAssets - The assets manager. {
	engine_ver:"0.1.0",
	category:"class",
} **/

/** beforeCreate - Fired before an asset is created. {
	category: "event",
	argument: {
		type:"object",
		name:"asset",
		desc:"The asset object this event was fired for.",
	},
} **/
/** afterCreate - Fired after an asset is created. {
	category: "event",
	argument: {
		type:"object",
		name:"asset",
		desc:"The asset object this event was fired for.",
	},
} **/
/** assetLoaded - Fired after an asset has been loaded into memory. This does not mean
that the assets image is ready to use. If you want to know when an asset image is ready
you can hook the assetReady event. {
	category: "event",
	argument: {
		type:"object",
		name:"asset",
		desc:"The asset object this event was fired for.",
	},
} **/
/** assetReady - Fired after an asset has loaded its image data into memory. {
	category: "event",
	argument: {
		type:"object",
		name:"asset",
		desc:"The asset object this event was fired for.",
	},
} **/
/** allAssetsLoaded - Fired after all assets in the load queue have finished loading. {
	category: "event",
} **/
IgeAssets = new IgeClass({
	
	Extends: [IgeCollection, IgeItem, IgeEvents],
	
	/** engine - A reference object to the main engine instance. {
		category:"property",
		type:"object",
		instanceOf:"IgeEngine",
	} **/
	engine: null,
	
	/** byIndex - An array with an integer index that allows you to access every
	item created using the create method in the order it was created. {
		category:"property",
		type:"array",
		index:"integer",
	} **/
	byIndex: null,
	
	/** byId - An array with a string index that allows you to access every item
	created using the create method by its id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byId: null,
	
	/** byMapId - An array with a string index that allows you to access every item
	created using the create method by its map_id. {
		category:"property",
		type:"array",
		index:"string",
	} **/
	byMapId: [],
	
	/** assetImage - An array with a string index that allows you to access every asset's
	image element (img tag) by the asset_id. {
		category:"property",
		type:"array",
		index:"string",
	} **/	
	assetImage: [],
	
	/** imagesLoading - An integer value representing the number of images left to load into
	memory. If set to zero then all images have finished loading. If -1 then no images have
	been loaded and no assets have been created. {
		category:"property",
		type:"integer",
	} **/	
	imagesLoading: null,
	
	/* CEXCLUDE */
	/** imageMagic - A reference to the ImageMagick node module instance. {
		category:"property",
		type:"object",
		flags:"server",
	} **/
	imageMagic: null,
	/* CEXCLUDE */
	
	/** init - The constructor for this class. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns a new instance of this class",
		},
		argument: {
			type:"object",
			name:"engine",
			desc:"The active engine instance.",
			instanceOf:"IgeEngine",
		},
	} **/
	init: function (engine) {
		this._className = 'IgeAssets';
		
		this.engine = engine;
		
		this.byIndex = [];
		this.byId = [];
		this.byMapId = [];
		this.assetImage = [];
		
		this.collectionId = 'asset';
		this.imagesLoading = -1;
		
		/* CEXCLUDE */
		if (this.engine.isServer) {
			this.imageMagic = require(igeConfig.mapUrl('/node_modules/imagemagick/imagemagick'));
		}
		/* CEXCLUDE */
		
		// Check if we have a network
		if (this.engine._IgeNetwork) {
			// Call the networkInit method of this class
			this.networkInit();
		}
	},
	
	networkInit: function () {
		// Network CRUD Commands
		this.engine.network.registerCommand('assetsCreate', this.bind(this.receiveCreate));
		//this.engine.network.registerCommand('assetsProcess', this.bind(this.processAssetsClient));
		this.engine.network.registerCommand('assetsUpdate', this.bind(this.receiveUpdate));
		this.engine.network.registerCommand('assetsRemove', this.bind(this.receiveRemove));
		
		this.engine.network.registerCommand('assetsStartSend', this.bind(this.netSendStarted));
		
		// Register standard property names to the network manifest see the API manual for more details
		this.engine.network.addToManifest([
			'asset_anchor_points',
			'asset_id',
			'asset_image_url',
			'asset_locale',
			'asset_render_mode',
			'asset_scale',
			'asset_sheet_enabled',
			'asset_sheet_frame',
			'asset_sheet_width',
			'asset_sheet_height',
			'asset_sheet_unit_x',
			'asset_sheet_unit_y',
			'asset_persist',
		]);
	},
	
	/** create - Create a new asset to be use in entities. {
		category:"method",
		return: {
			type:"object",
			desc:"Returns the newly created item on success or false on failure.",
		},
		arguments: [{
			type:"object",
			name:"asset",
			desc:"The asset object to be created.",
			link:"assetData",
		}, {
			type:"method",
			name:"callback",
			desc:"The method to call when the asset has been created successfully and its image data has also loaded into memory.",
		}],
	} **/
	create: function (asset, callback) {
		
		// Check if we need to auto-generate an id
		this.ensureIdExists(asset);
		
		// Check that the asset does not already exist!
		if (!this.byId[asset.asset_id]) {
			
			this.emit('beforeCreate', asset);
			
			// Create the local storage object
			asset.$local = asset.$local || {};
			
			// Load any template properties
			if (asset.template_id) { this.engine.templates.applyTemplate(asset, asset.template_id); }
			
			// Check if a callback was specified and if so, attach it to the item
			if (typeof callback == 'function') {
				asset.$local.$callback = callback;
			}
			
			// Set all loaded to false
			this.allLoaded = false;
			
			// Set some defaults
			asset.asset_scale = asset.asset_scale || 1;
			delete asset.processing;
			
			this.byIndex.push(asset);
			this.byId[asset.asset_id] = asset;
			this.byMapId[asset.map_id] = this.byMapId[asset.map_id] || [];
			this.byMapId[asset.map_id].push(asset);
			
			this.emit('afterCreate', asset);
			if (this.imagesLoading == -1) { this.imagesLoading = 0; }
			this.imagesLoading++;
			
			if (this.engine.isServer) {
				/* CEXCLUDE */
				this.processAssetsServer();
				/* CEXCLUDE */
			} else {
				// Client code
				this.log('Processing asset load for: ' + asset.asset_id);
				this.processAssetsClient();
			}
			
			return asset;
			
		} else {
			this.log('Attempted to create an asset that already exists in this collection with id "' + asset.asset_id + '"!', 'warning', {newAsset:asset, curAsset:this.byId[asset.asset_id]});
			return false;
		}
		
	},
	
	update: function () {},
	remove: function () {},
	
	/** assetReady - Check the ready state of an asset by it's id. {
		category:"method",
		return: {
			type:"bool",
			desc:"Returns true if the asset whose id is passed as the parameter has loaded its image and is ready to use. Returns false otherwise",
		},
		argument: {
			type:"string",
			name:"assetId",
			desc:"The id of the asset to be evaluated.",
		},
	} **/
	assetReady: function (assetId) {
		var ready = false;
		
		if (this.byId[assetId]) {
			if (this.byId[assetId].$local.$image) {
				ready = true;
			}
		}
			
		return ready;
	},
	
	/** processAssetsClient - Trigger loading of the current list of un-loaded assets. {
		category:"method",
	} **/
	processAssetsClient: function () {
		// Check if we are the client engine
		if (!this.engine.isServer && !this.engine.isSlave) {
			var asset = null;
			
			for (var i = 0; i < this.byIndex.length; i++) {
				asset = this.byIndex[i];
				if (!this.assetReady(asset.asset_id) && !asset.processing) {
					asset.processing = true;
					this.log('Loading asset image data: ' + asset.asset_image_url);
					this.loadImage(asset.asset_id, asset.asset_image_url);
				}
			}
		}
	},
	
	/** processAssetsServer - Trigger loading of the current list of un-loaded assets. {
		category:"method",
	} **/
	processAssetsServer: function () {
		if (this.engine.isServer) {
			/* CEXCLUDE */
			// Server doesn't load images via <img> tags, instead we grab image
			// data using the ImageMagick library
			var asset = null;
			
			for (var i = 0; i < this.byIndex.length; i++) {
				
				asset = this.byIndex[i];
				
				if (!this.assetReady(asset.asset_id) && !asset.processing) {
					this.log('Asset needs loading: ' + asset.asset_id);
					asset.processing = true;
					
					// Support asset urls outside of the main game folder such as those used in modules
					var finalUrl = igeConfig.mapUrl(asset.asset_image_url);
					
					// Load the asset and get it's details
					try {
						this.imageMagic.identify(finalUrl, this.bind(function(err, data){
							if (!err) {
								delete asset.processing;
								// Assign the data to the image for later use
								this.log('Asset loaded successfully: ' + asset.asset_id);
								this._setAssetSize(asset, {width:data.width, height:data.height});
								this.imagesLoading--;
								
								this.emit('assetLoaded', asset);
								if (this.imagesLoading == 0) { this.emit('allAssetsLoaded'); }
							} else {
								this.log('Cannot execute imagemagick "identify". Is the image file valid and is imagemagick installed?', 'error', [asset, err]);
							}
						}));
					} catch (err) {
						this.log('Cannot execute imagemagick "identify". Is the image file valid and is imagemagick installed?', 'error', err);
					}
					
				}
				
			}
			/* CEXCLUDE */
		}
	},
	
	/** generateMipMap - Generates a new MIP based upon the specified asset and mipScale. {
		category:"method",
		engine_ver:"0.3.0",
		return: {
			type:"object",
			desc:"Returns the MIP as a canvas element or false on error.",
		},
		arguments: [{
			type:"object",
			name:"asset",
			desc:"The asset to use when generating the MIP.",
		}, {
			type:"integer",
			name:"mipScale",
			desc:"The scale to draw the MIP at. Defaults to 0.5.",
			flags:"optional",
		}],
	} **/
	generateMipMap: function (asset, mipScale) {
		if (!mipScale) { mipScale = 0.5; }
		var img = null;
		if (asset.$local.$image) {
			img = asset.$local.$image;
		} else {
			this.log('No image was passed and no image exists in the asset for generateMipMap.', 'warning', asset);
		}

		if (img) {
			var canvas = document.createElement('canvas');
			canvas.width = img.width * mipScale;
			canvas.height = img.height * mipScale;
			
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
			
			return canvas;
		}
		
		return false;
	},
	
	/** loadImage - Loads an image into an img tag and sets an onload event to capture
	when the image has finished loading. {
		category:"method",
		arguments: [{
			type:"string",
			name:"assetId",
			desc:"The asset_id of the asset to load the image for.",
		}, {
			type:"string",
			name:"imageUrl",
			desc:"The image url used to load the image data.",
		}],
	} **/
	loadImage: function (assetId, imageUrl) {
		var newImage = new IgeElement('img', {id:assetId});
		newImage.onload = this.bind(this._assetImageLoaded);
		newImage.src = imageUrl;
		this.assetImage[assetId] = newImage;
	},
	
	/** _assetImageLoaded - Called when an image has finished loading. Stores the new image in the
	asset and emits an assetLoaded event. {
		category:"method",
		arguments: [{
			type:"object",
			name:"e",
			desc:"The event object.",
		}],
	} **/
	_assetImageLoaded: function (e) {
		var img = e.target;
		var asset = this.byId[img.id];
		delete asset.processing;
		this._setAssetSize(asset, img);
		
		this.imagesLoading--;
		this.log('Asset image loaded: ' + img.src + ' with dimensions (' + img.width + ', ' + img.height + ')');
		
		// Emit the asset loaded event with the asset id - useful for deferring parts of the code until an asset exists
		this.emit('assetLoaded', asset);
		if (this.imagesLoading == 0) { this.emit('allAssetsLoaded'); }
	},
	
	/** _setAssetSize - Called when an asset's image has fully loaded and calculates and stores
	a number of useful values that are used to render the asset image and determine dimensions. {
		category:"method",
		arguments: [{
			type:"object",
			name:"asset",
			desc:"The asset object to use when setting object properties to.",
		}, {
			type:"object",
			name:"img",
			desc:"The image element whose dimensions are to be evaluated.",
		}],
	} **/
	_setAssetSize: function (asset, img) {
		// Check if we are loading a sheet and if so, pre-calculate
		// all the co-ordinates so that the renderer can use them
		// without having to calculate them itself every frame...
		var tempImageWidth = img.width;
		var tempImageHeight = img.height;
		
		if (asset.asset_sheet_enabled && asset.asset_sheet_width && asset.asset_sheet_height)
		{
			
			asset.asset_sheet_unit_x = tempImageWidth / asset.asset_sheet_width;
			asset.asset_sheet_unit_y = tempImageHeight / asset.asset_sheet_height;
			
			// Check the asset for data that we might need to calculate
			if (asset.asset_sheet_width > 1 || asset.asset_sheet_height > 1) {
				
				if (asset.asset_anchor_points[0]) {
					
					// Check that all the asset anchor points are filled in. If not, calculate them from the first one
					for (var i = 2; i <= (asset.asset_sheet_width * asset.asset_sheet_height); i++) {
		
						if (!asset.asset_anchor_points[i - 1]) {
							
							/*
							// Anchor point data missing, calculate it
							var yPos = (Math.ceil(i / asset.asset_sheet_width) - 1);
							var sourceY = asset.asset_sheet_unit_y * yPos;
							
							var xPos = ((i - (asset.asset_sheet_width * yPos)) - 1);
							var sourceX = asset.asset_sheet_unit_x * xPos;
							
							asset.asset_anchor_points[i - 1] = [sourceX + Number(asset.asset_anchor_points[0][0]), sourceY + asset.asset_anchor_points[0][1]];
							*/
							
							// Make the anchor point the same as the first one
							asset.asset_anchor_points[i - 1] = asset.asset_anchor_points[0];
						}
						
					}
					
				} else {
					
					this.log('Asset loaded with sheet enabled but no anchor point found. At least one must be defined.', 'error');
					
				}
				
			}
			
			asset.$local.$size = asset.$local.$size || [];
			
			// Calculate and store the asset size data
			asset.$local.$size[0] = asset.asset_sheet_unit_x;
			asset.$local.$size[1] = asset.asset_sheet_unit_y;
			
		} else {
			
			asset.$local.$size = asset.$local.$size || [];
			
			// Single image so use source as whole image size
			asset.$local.$size[0] = tempImageWidth;
			asset.$local.$size[1] = tempImageHeight;
			
		}
		
		asset.$local.$imageWidth = tempImageWidth;
		asset.$local.$imageHeight = tempImageHeight;
		asset.$local.$image = img;
		
		// Check if the asset has a callback method and if so, fire it
		if (typeof asset.$local.$callback == 'function') {
			asset.$local.$callback.apply(this, [asset]);
			asset.$local.$callback = null;
		}
		
		this.emit('assetReady', asset);
	},
	
});