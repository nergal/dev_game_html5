ig.module(
    'plugins.dialogs'
).
requires(
    'impact.impact',
    'impact.game'
).
defines(function() {

ig.Dialogs = ig.Class.extend({

      backLevel: null,
      entities: [],
      yDelta: 15,

      init: function() {
        ig.dialogs = this;
      },

      spawnComponent: function(caption, action, options) {
        options = options || {};
        var comp;
        if (options.button) {
          comp = new ig.Dialogs.DialogButton(10, this.yDelta, caption, action, options);
        } else if (caption) {
          comp = new ig.Dialogs.DialogText(10, this.yDelta, caption, action, options);
        } else if (options.icon) {
          comp = new ig.Dialogs.DialogIcon(10, this.yDelta, options.icon);
        }
        if (comp) {
//          this.entities = this.entities || [];
          this.entities.push(comp);
          this.yDelta += comp.size.y + 10;
        }
        return comp;
      },

      newDialog: function() {
        this.removeEntities();
        ig.gameState.showMenu = true;
        ig.gameState.canClick = false;
        return(this);
      },

      // options: color, icon, iconAllign
      addButton: function(caption, action, options) {
        if (caption == "Cancel") {
          options = options || {};  // undefined case
          options.color = options.color || "rgba(180,180,180, 0.8)";
        }
        options = options || {};
        ig.gameState.showMenu = true;
        ig.gameState.canClick = false;
        if (!action) {
          options.color = options.color || "rgba(0,180,0, 0.8)";
        }
        options.button = true;
        this.spawnComponent(caption, action, options);
      },

      // options: color, icon
      addLabel: function(caption, options) {
        ig.gameState.showMenu = true;
        ig.gameState.canClick = false;
        options = this.defOption(options, {noBorder: true} );
        this.spawnComponent(caption, null, options);
      },

      // todo - add to object prototype
      defOption: function(opts, plus) {
        opts = opts || {};
        for (key in plus) {
          opts[key] = plus[key];
          console.log("key", key, plus[key]);
        }
        return(opts);
      },

      addTitle: function(caption, options) {
        options = this.defOption(options, {color: '#1693E2'} );
        options = this.defOption(options, {font: ig.game.fonts.droid_sans_white_30} );
        if (typeof(options.noBorder) === 'undefined' ) {
          options.noBorder = true;
        }
        this.addLabel(caption, options);
      },

      addText: function(text, options) {
        options = options || {};
        options.multiline = true;
        this.spawnComponent(text, null, options);
      },

      addIcon: function(icon) {
        this.spawnComponent(null, null, {icon: icon});
      },

      addBackButton: function(caption) {
          this.addButton(caption || '< back');
      },

      update: function() {
        this.buttonPressed = false;
        this.entities.forEach(function(ent) {
          ent.update();
        });

        if (ig.input.pressed('click') && !this.buttonPressed) {
            this.hideDialog();
        }
      },

      //todo - have this fade in/out?
      fadeBackground: function(col) {
        ig.system.context.fillStyle = "rgba(255,255,255, 0.8)";
        ig.game.roundedRec(ig.system.context, 5, 5, 310, 470, 10, true);
      },

      draw: function() {
        if (this.entities.length > 0) {
          this.fadeBackground();
        }

        this.entities.forEach(function(ent) {
          ent.draw();
        });
      },

      removeEntities: function() {
        var ent = this.entities.shift();
        while(ent) {
          ent.kill();
          ent = this.entities.shift();
        }
      },

      hideDialog: function(func) {
        ig.gameState.canClick = false;
        ig.gameState.showMenu = false;
        this.yDelta = 10;
        this.removeEntities();
        if (func)
          func();
      }
    });


    ig.Dialogs.DialogElement = ig.Entity.extend ({
        size: {x: 300, y: 40},
        type: ig.Entity.TYPE.NONE,
        checkAgainst: ig.Entity.TYPE.NONE,
        collides: ig.Entity.COLLIDES.NONE,

        init: function(x, y, text, action, options) {
          if (typeof(text) == 'undefined') {
            console.log("ERROR: formatText: undefined");
            text = "----";
          }

          options = options || {};
          this.font = options.font || ig.game.fonts.standard;
          this.textWidth = ig.system.width - 30;

          this.parent(x, y);
          if (this.animSheet) {
            this.addAnim('idle', 1, [0], true);
            this.currentAnim = this.anims.idle;
          }

          if (options.multiline) {
            this.text = this.formatText(text);
          } else {
            this.text = [text];
          }
          this.spaceBetweenLines = 3;

          this.size.y = (this.font.height + this.spaceBetweenLines) * this.text.length + 30;

          if (options.icon) {
            var dy = (this.size.y > Icon.edge) ? (this.size.y - Icon.edge) / 2 : 0;
            this.icon = new Icon(this.pos.x, this.pos.y + dy, options.icon);
            this.textWidth -= Icon.edge;
          }

          this.ctx = ig.system.context;
          this.noBorder = options.noBorder;
          this.action = action;
          this.button = options.button;
          this.color = options.color || "rgba(255,255,255, 0.8)";
        },

        formatText: function(text) { // splits long string into multiple lines
          var words = text.split(" ");
          var lines = [];
          var line = [];
          var length = 0;
          var spaceLength = this.font.widthForString(' ');
          for (var i=0; i < words.length; i++) {
            var currentLength = this.font.widthForString(words[i]);
            if (length + currentLength < this.textWidth) {
              length += currentLength + spaceLength;
              line.push(words[i]);
            } else {
              lines.push(line.join(" "));
              line = [words[i]];
              length = currentLength;
            }
          }
          lines.push(line.join(" "));
          return lines;
        },

        draw: function() {
          if (typeof(this.text) == 'undefined') {
            console.log("ERROR: draw no text");
          }

          this.ctx.fillStyle = this.color;
          ig.game.roundedRec(this.ctx, this.pos.x, this.pos.y, this.size.x - this.pos.x + 10, this.size.y, 5, this.noBorder);
          for (var i in this.text) {
            this.font.draw(this.text[i], this.pos.x + (this.icon ? this.icon.textOffset : 3), this.pos.y + 15 + (i * (this.font.height + this.spaceBetweenLines)));
          }
          if (this.icon) this.icon.draw();
        },

        update: function(){
          if (this.icon) {
            this.icon.update();
          }

          if (ig.input.pressed('click') && this.inFocus()) {
            ig.dialogs.buttonPressed = true;
          }
        },

        inFocus:function () {
          if (!ig.gameState.canClick) { return false; }

          xmax = this.pos.x + this.size.x;
          ymax = this.pos.y + this.size.y;

          xhit = (this.pos.x <= ig.input.mouse.x && ig.input.mouse.x <= xmax );
          yhit = (this.pos.y <= ig.input.mouse.y && ig.input.mouse.y <= ymax );
          flag = xhit && yhit;

          if (flag) {
            ig.gameState.canClick = false;
          }

          return(flag);
        }

    });

    ig.Dialogs.DialogButton = ig.Dialogs.DialogElement.extend({
      update:function () {
        if (this.icon) {
          this.icon.update();
        }
        if (ig.input.pressed('click') && this.inFocus()) {
          if (this.button) {
            ig.dialogs.hideDialog();
            if (typeof(this.action) === 'function') {
              this.action();
            }
          }
          ig.dialogs.buttonPressed = true;
        }
      }
    });

    ig.Dialogs.DialogText = ig.Dialogs.DialogElement.extend({

    });
    ig.Dialogs.DialogIcon = ig.Dialogs.DialogElement.extend({
      iconsMap: {
        guide: {file: "guide/guide-dialog.png", frames: [0], frameTime: 0.1, size: {x: 128, y: 128}},
        'quest-cleared': {file: "messages/quest-cleared.png", frames: [0, 1], frameTime: 0.1, size: {x: 320, y: 128}}
      },
      size: {x: 0, y: 0},

      init: function(x, y, icon) {
        this.pos = {x: x, y: y};

        if (typeof(icon.draw) === 'function') {
          this.icon = icon;
          this.icon.update = function(){};
          this.size = {x:icon.size.x, y:icon.size.y};
        } else {
          var map = this.iconsMap[icon];
          if (map) {
            var anim = new ig.AnimationSheet('media/dialogs/' + map.file, map.size.x, map.size.y);
            this.icon = new ig.Animation(anim, map.frameTime, map.frames, false);
            this.size = {x:map.size.x, y:map.size.y};
          }
        }
      },

      draw: function(){
        if (this.icon) {
          this.icon.draw(this.pos.x, this.pos.y);
        }
      }
    });
});