 ig.module( 
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
	'impact.system',
	'game.entities.puck',
	'game.entities.paddle-cpu',
	'game.entities.paddle-player',
	'game.entities.ogre',
	'game.entities.baby',
	'game.entities.bear',
	'game.entities.building-001',
	'game.levels.test',
	'game.levels.main',
	'game.levels.test02',
	'plugins.notification-manager'
)
.defines(function(){

MyGame = ig.Game.extend({
	mouseLast: {x:0, y:0},
	
	// Load a font
	font: new ig.Font( 'media/04b03.font.png' ),
	//gravity: 100,
	mNoteManager: new ig.NotificationManager(), 
	
	init: function() {
		ig.input.bind( ig.KEY.UP_ARROW, 'up' );
		ig.input.bind( ig.KEY.DOWN_ARROW, 'down' );
		ig.input.bind( ig.KEY.LEFT_ARROW, 'left');
		ig.input.bind( ig.KEY.RIGHT_ARROW, 'right');
		ig.input.bind (ig.KEY.MOUSE1, 'mouseLeft'); 
		this.loadLevel( LevelTest02 );
		//this.loadLevel( LevelMain );
	},
	
	update: function() {
		// Update all entities and backgroundMaps
		this.parent();
		
		// Add your own, additional update code here
		if (ig.input.pressed('mouseLeft')) {
			
			// set the starting point
			this.mouseLast.x = ig.input.mouse.x;
			this.mouseLast.y = ig.input.mouse.y;	
		}
		
		if (ig.input.state('mouseLeft')) {
			this.screen.x -= ig.input.mouse.x - this.mouseLast.x;
			this.screen.y -= ig.input.mouse.y - this.mouseLast.y;
			this.mouseLast.x = ig.input.mouse.x;
			this.mouseLast.y = ig.input.mouse.y;		  
		}
		this.mNoteManager.update(); 
	},
	
	draw: function() {
		// Draw all entities and backgroundMaps
		this.parent();
		this.mNoteManager.draw(); 
	}
});

//console.log (document.body.clientWidth);
var originalWidth = 1280;
var originalHeight = 720;
var screenWidth = document.body.clientWidth;
var scale = Math.max(1,Math.round((screenWidth/originalWidth)*4)/4);

ig.main( '#canvas', MyGame, 60, originalWidth,originalHeight,1);

});
