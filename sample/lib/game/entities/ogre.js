ig.module(
	'game.entities.ogre'
)
.requires(
	'impact.entity'
)
.defines(function(){

EntityOgre = ig.Entity.extend({
	
	size: {x:256, y:256},
	collides: ig.Entity.COLLIDES.PASSIVE,
	
	animSheet: new ig.AnimationSheet( 'media/ogre.png', 256, 256 ),
	
	bounciness: 1,
	//_wmDrawBox: true,
	//_wmBoxColor:'#ff00ff',
	
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		
		this.addAnim( 'right', 0.1, [40,41,42,43,44,45,46,47] );
		this.addAnim( 'left', 0.1, [8,9,10,11,12,13,14,15] );
		this.addAnim( 'up', 0.1, [24,25,26,27,28,29,30,31] );
		this.addAnim( 'down', 0.1, [56,57,58,59,60,61,62,63] );

		
		this.vel.x = 30;
	},
    
    update: function() {
		if (ig.input.pressed('up')){
			this.vel.y = -20; this.vel.x = 0; 
			this.currentAnim = this.anims.up;	
		} else if (ig.input.pressed('down')) {
			this.vel.y = 20; this.vel.x = 0;
			this.currentAnim = this.anims.down;	
		} else if (ig.input.pressed('right')){
			this.vel.x = 20; this.vel.y = 0;
			this.currentAnim = this.anims.right;	
		} else if (ig.input.pressed('left')) {
			this.vel.x = -20; this.vel.y = 0;
			this.currentAnim = this.anims.left;	
		}
        this.parent(this);
    },
	
	handleMovementTrace: function (res) {
		if (res.collision.x || res.collision.y) {
			console.log ('pos('+res.pos.x+','+res.pos.y+'),tile('+res.tile.x+','+res.tile.y+')');
			this.vel.x = this.vel.y = 0;
		}
		this.parent(res);
	}
});

});