ig.module(
	'game.entities.bear'
)
.requires(
	'impact.entity'
)
.defines(function(){

EntityBear = ig.Entity.extend({
	
	size: {x:128, y:128},
	collides: ig.Entity.COLLIDES.PASSIVE,
	
	animSheet: new ig.AnimationSheet( 'media/werebear_white.png', 128,128 ),
	
	bounciness: 1,
	
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		
		this.addAnim( 'idle', 0.1, [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17] );
		
		this.vel.x = 10;
	},
    
    update: function() {
        this.parent();
    },
	
	handleMovementTrace: function (res) {
		if (res.collision.x || res.collision.y) {
			console.log ('pos('+res.pos.x+','+res.pos.y+'),tile('+res.tile.x+','+res.tile.y+')');
			this.vel.x = this.vel.y = 0;
		}
		this.parent(res);
	}
});

});