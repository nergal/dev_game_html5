ig.module(
	'game.entities.baby'
)
.requires(
	'impact.entity',
	'plugins.bezierpath.bezierpath'
)
.defines(function(){

EntityBaby = ig.Entity.extend({
	
	size: {x:32, y:64},
	collides: ig.Entity.COLLIDES.PASSIVE,
	
	animSheet: new ig.AnimationSheet( 'media/child_sprite_model.png', 32, 64 ),
	
	bounciness: 1,
	
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		this.origin = {x:x, y:y}; 
		this.addAnim( 'right', 0.1, [16,17,18,19,20,21,22,23] );
		this.addAnim( 'left', 0.1, [24,25,26,27,28,29,30,31] );
		this.addAnim( 'up', 0.1, [0,1,2,3,4] );
		this.addAnim( 'down', 0.1, [8,9,10,11,12] );
		
		//this.vel.x = 10;
		//this.vel.y = 10;
		
		pathNodes = [ { bump: {x: 0, y: 0} },  { pull1: {x: 100, y: 100}, pull2: {x:200, y: 200}, bump: {x: 300, y: 300}, inc: 0.001 } ];
	    this.bezPath = new ig.Bezierpath(pathNodes, {cycle: 1, oscillate: true});
		this.bezPath.loop = true;
	},
    
    update: function() {
		if (this.bezPath.direction == 0) {
			this.currentAnim = this.anims.right;	
		} else {
			this.currentAnim = this.anims.left;	
		}
        this.parent();
	    newPos = this.bezPath.nextPos();
		this.pos.x = this.origin.x + newPos.x;
	    this.pos.y = this.origin.y + newPos.y;
    },
	
	handleMovementTrace: function (res) {
		if (res.collision.x || res.collision.y) {
			//console.log ('pos('+res.pos.x+','+res.pos.y+'),tile('+res.tile.x+','+res.tile.y+')');
			//this.vel.x = this.vel.y = 0;
		}
		this.parent(res);
	}
});

});