ig.module(
	'game.entities.building-001'
)
.requires(
	'impact.entity',
	'plugins.dialogs'
)
.defines(function(){

EntityBuilding001 = ig.Entity.extend({
	
	size: {x:160, y:150},
	collides: ig.Entity.COLLIDES.PASSIVE,
	clicked: false, 
	animSheet: new ig.AnimationSheet( 'media/building02.png', 162,158 ),
	
	bounciness: 1,
	
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		
		this.addAnim( 'idle', 0.1, [0] );
		
				
	},
    
    update: function() {
	var width = this.size.x, height = this.size.y;
	var pos = {x: ig.input.mouse.x, y:ig.input.mouse.y};
	if ( ig.input.pressed('mouseLeft')) {
		if (
	    (pos.x > this.pos.x && pos.x < (this.pos.x + this.size.x)) &&
	    (pos.y > this.pos.y && pos.y < (this.pos.y + this.size.y))) { 
			if(this.clicked==false)
			this.currentAnim.sheet.image.brighter(100);
			this.clicked = true;
			//ig.game.mNoteManager.spawnNote(new ig.Font( 'media/font-medium.png' ),
			//							   'my message', this.pos.x, this.pos.y,
			//							   {vel:{x:0,y:0}, alpha:0.5, lifetime:2.2, fadetime:0.3});
			//this.showDialog();
			$("#dialog").html('<p>this is conents...</p><br>').dialog({
		
				title: 'Hotel : $5,000',
				modal: false
			}); 
		}
		else {
			if (this.clicked==true)
			this.currentAnim.sheet.image.brighter(-100);
			this.clicked = false;
			
			
		}
	}
        this.parent();
    },
	
	showDialog: function() {
		ig.dialogs.addTitle('tile bar');
		ig.dialogs.addLabel('label');
		ig.dialogs.addButton('button');
	}
});

});